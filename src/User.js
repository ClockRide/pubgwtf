import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {loadData} from './helpers/loadData';

export class User extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: JSON.parse(localStorage.getItem('user'))
        };
        this.getUser();
    }

    getUser() {
        loadData('/getUser')
        .then(data => {
            const { user } = data;
            this.setState({
                user
            });
            localStorage.setItem('user', JSON.stringify(user));
        })
    }



    render() {
        const { user } = this.state;
        
        return (
            <div className="login_block">
                { user === null ? (
                    <a href="/login" className="login_off">Войти через STEAM</a>
                ) : (
                    <div className="profile">
                        <div className="user_photo"><img src={user.avatar} /></div>
                        <div className="desc_user_profile">
                            <div className="nikname">{user.username}</div>
                            <Link to={`/profile/${user.steamid64}`} className="prof_link">Профиль</Link>
                            <Link to="/inventory" className="inven_link">Инвентарь</Link>
                        </div>
                        <a href="/logout" className="login_out">Выйти</a>
                    </div>
                )}
            </div>
        );
    }
}