import React, { Component } from 'react';
import { loadData } from '../helpers/loadData';
import { ToastContainer, toast } from 'react-toastify';
import { Link } from 'react-router-dom';

const TICKETS_STATES = {
    0: 'Waiting',
    1: 'Closed'
}

export class Support extends Component {
    constructor(props) {
        super(props);

        this.state = {
            tickets: [],
            ticketText: ''
        };
        this.loadTickets();
    }

    loadTickets() {
        loadData('/getTickets')
        .then(data => {
            const { tickets } = data;
            this.setState({ tickets })
        })
        .catch(err => {
            toast('Ошибка загрузки тикетов', {
                type: toast.TYPE.ERROR
            });
        })
    }

    createTicket(event) {
        event.preventDefault();
        const { ticketText } = this.state;
        loadData('/createTicket', { ticketText })
        .then(data => {
            toast(data.text, {
                type: data.success ? toast.TYPE.SUCCESS : toast.TYPE.ERROR
            });
            if (data.success) {
                this.setState({ ticketText: '' });
                this.loadTickets();
            }
        })
        .catch(err => {
            toast('Ошибка создания тикета', {
                type: toast.TYPE.ERROR
            });
        })
    }

    render() {
        return (
            <div className="card-block">
                <ToastContainer />
                <div class="title-row d-flex justify-content-center mb-3">Поддержка</div>
                {/* <div className="card">
                    <h5 className="card-title">FEEDBACK</h5>
                    <div className="card-body">
                        <p className="card-text">Просьба сразу же писать суть вопроса - сообщения типа:</p>
                        <p className="card-text">"Привет, Как дела", "Ты тут?" и просто добавление в друзья без сообщения будут игнорироваться - берегите свое и наше время! Обратите внимание, что если Вам кто-то пытается продать что-то с нашего сайта, скорее всего это мошенник</p>
                    </div>
                </div> */}
                <div className="card ">
                    <div className="card-body">
                        <p className="mb-2">Если у вас есть какие-то вопросы напишите нам. Мы ответим вам в течении 12 часов</p>
                        <div className="d-flex justify-content-center flex-column flex-md-row justify-content-md-between mb-3">
                        <div className=" text-center  text-md-left mb-md-1 mb-3">
                            <a href="#" className="btn btn-default btn-sm">Создайте новый тикет</a>
                        </div>
                        <div className="ref-rezult text-center text-md-right mb-md-1 mb-3 ">
                            <div className="text-small">Нажмите на номер вашего тикета что-бы открыть его</div>
                        </div>
                        </div>
                        <div className="table-responsive ">
                            <table className="table table-striped table-top  table-support">
                                <thead>
                                <tr>
                                    <th scope="col " className="col-id ">ID</th>
                                    <th scope="col ">Последнее сообщение</th>
                                    <th scope="col ">Тема</th>
                                    <th scope="col ">Статус</th>
                                </tr>
                                </thead>
                                <tbody>
                                    {this.state.tickets.map(ticket => {
                                        const message = ticket.messages[ticket.messages.length - 1];
                                        if (message.user === null) message.user = {
                                            username: 'Support',
                                            avatar: '/assets/img/support-avatar.png'
                                        }
                                        return (
                                            <tr>
                                                <td><Link to={`/support/${ticket.id}`}>#{ticket.id}</Link></td>
                                                <td>
                                                    <Link to={`/support/${ticket.id}`} className="link-line">
                                                        <img src={message.user.avatar} className="avatar" alt />
                                                        {message.text}
                                                    </Link>
                                                </td>
                                                <td>{ticket.subject}</td>
                                                <td><span className="status ">{TICKETS_STATES[ticket.state]}</span></td>
                                            </tr> 
                                        )
                                    })}                  
                                </tbody>
                            </table>
                        </div>
                        <form onSubmit={this.createTicket.bind(this)} className="refer-form">
                            <div className="row mb-4">
                                <div className="col-12 col-lg-8 offset-lg-2">
                                <div className="form-group">
                                    <textarea value={this.state.ticketText} onChange={event => this.setState({ticketText: event.target.value})} className="form-control" placeholder="Input text" id="text" rows={5}/>
                                </div>
                                <div className="row">
                                    <div className="col-sm-6 mb-3 mb-sm-0">
                                    <button type="submit" className="btn btn-default btn-block">Отправить</button>
                                    </div>
                                    <div className="col-sm-6 mb-3 mb-sm-0">
                                    <button type="reset" className="btn btn-default btn-block ">Закрыть обращение</button>
                                    </div>
                                </div>
                                </div>
                            </div>
                            </form>
                    </div>
                </div>
            </div>
        )
    }
}