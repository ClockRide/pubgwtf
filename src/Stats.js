import React, { Component } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import { getFromLocalStorage } from './helpers/getFromLocalStorage';
import { setToLocalStorage } from './helpers/setToLocalStorage';
import { loadData } from './helpers/loadData.js';

import openSocket from 'socket.io-client';

export class Stats extends Component {
    constructor(props) {
        super(props);

        const {
            uniqGamePlayers,
            gamesToday,
            maxBankToday,
            itemsRaffledToday
        } = getFromLocalStorage(['uniqGamePlayers', 'gamesToday', 'maxBankToday', 'itemsRaffledToday']);
        this.state = {
            uniqGamePlayers,
            gamesToday,
            maxBankToday,
            itemsRaffledToday
        };
        this.loadStats();
    }

    componentWillUnmount() {
        ['newGame'].forEach(item => this.socket.off(item));
    }

    componentDidMount() {
        this.socket = openSocket(':7020', {
            transports: ['websocket']
        });
        this.socket.on('newGame', this.loadStats.bind(this));
    }

    loadStats() {
        loadData('/getStats')
        .then(data => {
            this.setState(data);
            setToLocalStorage(data);
        })
        .catch(err => {
            toast('Ошибка загрузки статистики', {
                type: toast.TYPE.ERROR 
            });
        });
    }

    render() {
        const {
            uniqGamePlayers,
            gamesToday,
            maxBankToday,
            itemsRaffledToday
        } = this.state;
        
        return (
            <div className="block_stat">
                <div className="stat1"><p><span className="stats-onlineNow">0</span></p>сейчас онлайн</div>
                <div className="stat1"><p>{ gamesToday }</p>игр сегодня</div>
                <div className="stat1"><p>{ maxBankToday } <span className="coins big" /></p>максимально</div>
            </div>
        )
    }
}