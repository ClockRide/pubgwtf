import React, { Component } from 'react';
import { TradeLink } from '../Roulette/TradeLink';
import { loadData } from '../helpers/loadData';

export class Settings extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: JSON.parse(localStorage.getItem('user'))
        }
        this.loadUser();
    }

    loadUser() {
        loadData('/getUser')
          .then(data => {
              const { user } = data;
              this.setState({
                  user
              });
              localStorage.setItem('user', JSON.stringify(user));
          })
    }

    render() {
        const { user } = this.state;
        return (
            <div>
                <div class="title-row d-flex justify-content-center mb-2">
                    Настройки
                </div>
                <TradeLink user={user} />
            </div>
        )
    }
}