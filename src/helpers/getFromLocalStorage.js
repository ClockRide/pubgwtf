export const getFromLocalStorage = (items, emptyArrayIfNull) => {
    let data = {};

    for (const item of items) {
        data[item] = localStorage.getItem(item);
        if (data[item] === null && emptyArrayIfNull) data[item] = [];
    }

    return data;
}