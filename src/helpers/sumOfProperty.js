export const getSumOfProperty = (array, property) => {
    return array.reduce((prev, curr) => {
        return prev + parseFloat(curr[property]);
    }, 0);
}