export const GameConfig = {
    prefinishedTimer: 15,
    itemsCount: 100,
    minDeposit: 0.5,
    maxItemsCount: 40,
    maxItemsTrade: 20,
    timer: 120,
    appID: 578080
}