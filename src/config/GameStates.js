export const GameStates = {
    CREATED: 0,
    STARTED: 1,
    PRIFINISHED: 2,
    FINISHED: 3,
    0: 'CREATED',
    1: 'STARTED',
    2: 'PREFINISHED',
    3: 'FINISHED'
}