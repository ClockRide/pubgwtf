import React, { Component, Fragment } from 'react';
import { Stats } from './Stats';
import { User } from './User';

export class ContentHeader extends Component {
    render() {
        return (
            <Fragment>
                <Stats />
                <User />
                <div className="clr" />
            </Fragment>
        )
    }
}