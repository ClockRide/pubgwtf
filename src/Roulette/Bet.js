import React, { Component, Fragment } from 'react';

export class Bet extends Component {
    render() {
        const { bet } = this.props;

        return (
            <div className="deposit_user">
                <div className="head_block_deposit">
                    <img src={bet.user.avatar} />
                    <a href={`/user/${bet.user.steamid64}`} className="nikname_deposit_user">{bet.user.username}</a> внес <span>{bet.itemsCount}</span> предметов <span className="mon_deposit_u">{bet.price} <span className="coins white" /></span> (шанс: <span><b className="id-aZvtX5Vaes">{bet.chance}%</b></span>)
                    <div className="bilety_rdep">Билеты: от <span>#{bet.ticket_from}</span> до <span>#{bet.ticket_to}</span></div>
                </div>
                <div className="clr" />
                <div className="items_block_deposit">
                    {bet.items.map(item => (
                        <div className="weapon_deposit" market_hash_name title={item.name} data-toggle="tooltip">
                            <img src={item.image} />
                            <div className="price_weapon_deposit"><b>{item.price.toFixed(2)}</b> <span className="coins white" /></div>
                        </div>
                    ))}
                    <div className="clr" />
                </div>
            </div>
        )
    }
}