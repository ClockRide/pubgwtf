import React, { Component, Fragment } from 'react';
import openSocket from 'socket.io-client';
import { GameConfig } from '../config/GameConfig';
import { GameStates } from '../config/GameStates';
import { getItemImage } from '../helpers/getItemImage';
import { Slider } from './Slider';
import { Link } from 'react-router-dom';
import { Timer } from './Timer';

const socket = openSocket(':7020', {
    transports: ['websocket']
});

export class EndGame extends Component {
    constructor(props) {
        super(props);

        this.state = {
            timer: this.props.historyGame ? 0 : GameConfig.prefinishedTimer
        }
    }

    formatNumber(number) {
        number = number.toString();
        if (number.length === 0) return '00';
        if (number.length === 1) return '0' + number;
        return number; 
    }

    getMinutes() {
        return this.formatNumber(Math.floor(this.state.timer / 60));
    }
    
    getSeconds() {
        return this.formatNumber(this.state.timer - this.getMinutes() * 60);
    }

    componentDidMount() {
        socket.on('timer', this.handleTimerChange.bind(this))
    }

    getWinnerItem() {
        const { winnerBet } = this.props.game;
        const items = JSON.parse(winnerBet.items);
        return getItemImage(items[0].classid);
    }

    handleTimerChange(timer) {
        if (this.props.historyGame) return;
        this.setState({
            timer
        })
    }

    render() {
        const { timer } = this.state;
        const { game, historyGame } = this.props;
        const resultsVisible = timer < 10 || historyGame;
        return (
            <Fragment>
                <div className="clr" />
                <div id="usersCarouselConatiner">
                    <div className="rullet_block_user ">
                        <Slider game={game} historyGame={historyGame} resultsVisible={resultsVisible} winningItem={this.getWinnerItem()} />
                    </div>
                    <div className="ukazatel" />
                </div>
                <div className="clr" />
                <div id="winnerInfo" className="win_desc_user">
                    <div className="winner-info-holder">
                    <ul>
                        <li>Победный билет: <span id="winTicket" className="win_bil">{game.winner_ticket}</span> (всего: <span id="totalTickets">{game.bets[0].ticket_to}</span>) <Link to="/fair" className="proverka_hesh check-hash">ПРОВЕРИТЬ</Link></li>
                        <li>Победил игрок: <img src={game.winner.avatar} /> <a id="winnerLink" className="nik_user_win">{game.winner.username}</a> (шанс: <span className="shance_win_user" id="winnerChance">{game.winnerChance}%</span>)</li>
                        <li>Выигрыш: <span id="winnerSum" className="winner_money">{game.price}</span> <span className="coins" /></li>
                    </ul>
                    <div className="right-block">
                        <div className="newGemaText">новая игра через</div>
                        <Timer />
                    </div>
                    <div className="clr" />
                    </div>
                </div>
                <div className="clr" />
                <div id="roundFinishBlock" className="game_end">
                    {/* <div class="date_game_start">06/04/2017 01:56</div> */}
                    <h3>Игра завершилась!</h3>
                    <p><Link to="/fair" className="she_g">ЧЕСТНАЯ ИГРА</Link> ЧИСЛО РАУНДА: <span className="number">{game.secret}</span></p>
                    <p className="secret_p">СЕКРЕТ: <span className="secret">{game.secret_word}</span><a href="/fairplay/166036" className="check-hash prov_secret">ПРОВЕРИТЬ</a></p>
                </div>
            </Fragment>
        )
    }
}