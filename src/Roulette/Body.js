import React, { Component } from 'react';
import { Bet } from './Bet';

export class Body extends Component {
    render() {
        const { game } = this.props;
        const usersInSlider = game.usersInGame.sort((a, b) => {
            if (a.chance > b.chance) return -1;
            if (a.chance < b.chance) return 1;
            if (a.steamid64 > b.steamid64) return -1;
            if (a.steamid64 < b.steamid64) return 1;
            return 0;
        });
        
        return (
            <div>
                <div id="usersChances" className="body_curent_user ">
                    {usersInSlider.map(user => (
                        <div className="current_user" data-original-title={user.username}>
                            <a href="/user/Jo38pVJA2g" target="_blank" className="user_ph_curr"><img src={user.avatar} /></a>
                            <div className="shance">{user.chance.toFixed(1)}%</div>
                        </div>                      
                    ))}
                </div>
                <div className="clr" />
                <div className="deposit_block">
                    {game.bets.map(bet => (
                        <Bet bet={bet} />
                    ))}
                </div>
                <div className="clr" />
                <div id="roundStartBlock" className="game_start">
                    <div className="date_game_start date">{game.created_at}</div>
                    <h3>Игра началась!</h3>
                    <p><a href="http://csgobomj.ru/fairplay" className="she_g">ЧЕСТНАЯ ИГРА</a> ХЭШ: <span id="hash">{game.hash}</span></p>
                </div>
            </div>
        )
    }
}