import React, { Component } from 'react';
import { GameConfig } from '../config/GameConfig';

export class Info extends Component {
    render() {
        return (
            <div className="card d-flex">
                <div className="row-line justify-content-center align-items-center d-flex flex-wrap flex-lg-nowrap justify-content-md-between">
                <div className="item">
                    PUBG skins exchange - <span class="highlight">PUBGWTF.COM</span>
                    <br /> The lowest fees!
                </div>
                <div className="item">
                    Add <span class="highlight">PUBGWTF.COM</span> to the nickname
                    <br /> and get <span class="highlight">-5%</span> from commission!
                </div>
                <div className="item">
                    Max bet - <span class="highlight">{GameConfig.maxItemsCount}</span> items
                    <br /> Min bet — <span class="highlight">{1}</span> item
                </div>
                </div>
            </div>
        )
    }
}