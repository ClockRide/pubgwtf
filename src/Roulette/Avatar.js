import React, { Component } from 'react';

export class Avatar extends Component {
    render() {
        return (
            <div className="current_user" data-original-title="Flesh CSGOFade.Net CSGOTitan.net">
                <a href="/user/t6MDmHJrKP" target="_blank" className="user_ph_curr"><img src="https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/ee/eef8c6d1f89866689d1fa0bc14b5b991f77a5efe_full.jpg" /></a>
                <div className="shance">40.8%</div>
            </div>
        );
    }
}