import React, { Component, Fragment } from 'react';
import { Timer } from './Timer';
import { GameConfig } from '../config/GameConfig';
// import { BotLink } from '../config/BotLink';
import { ToastContainer, toast } from 'react-toastify';
import { EndGame } from './EndGame';
import { GameStates } from '../config/GameStates';

export class Header extends Component {
    checkLink(event) {
        const { user } = this.props;
        if (user.access_token === null) {
            toast('Пожалуйста укажите трейд ссылку', {
                type: toast.TYPE.ERROR
            });
            event.preventDefault();
        }
    }

    render() {
        const { game, user } = this.props;
        let userChance = 0;
        if (user !== null && game.price > 0) {
            const userBetsSum = game.bets.filter(bet => bet.user_id == user.id).map(bet => bet.price).reduce((prev, current) => prev + parseFloat(current), 0);
            userChance = userBetsSum / (game.price / 100);
        }
        return (
            <Fragment>
                <div>
                    <div className="side_blok">
                        <div className="number_game_side">ИГРА <span>#<span id="roundId">{game.id}</span></span></div>
                        <div className="bank_game_side">БАНК <span className="roundBank">{game.price}</span> <span className="coins" /></div>
                        <div className="sound-control" />
                    </div>
                    {game.state !== GameStates.FINISHED ? (
                        <Fragment>
                            <div className="clr" />
                            <div id="barContainer" className="bar-container">
                                <div className="item-bar-wrap">
                                <div className="item-bar-text"><span>{game.items.length} / {GameConfig.itemsCount}</span> предмет</div>
                                <div className="item-bar" style={{width: `${game.items.length / (GameConfig.itemsCount / 100)}%`}}>
                                    <div className="progressbar-stripes" />
                                </div>
                                </div>
                                <div className="bar-text">или через</div>
                                <Timer />
                            </div>
                        </Fragment>
                    ) : ''}
                    
                    {game.state === GameStates.FINISHED ? (
                        <Fragment>
                            <div className="clr" />
                            <EndGame historyGame={this.props.historyGame} game={game} /> 
                        </Fragment>
                    ): ''}
                    <div className="clr" />
                    <div id="depositButtonsBlock">
                        <div id="depositButtons">
                        <div className="block_opisanie ">
                            <div className="comr">
                            Вы внесли <b><span id="myItemsCount">0 предметов</span></b><br />
                            Ваш шанс на победу: <b><span id="myChance">0%</span></b>
                            </div>
                            <div className="comr4">
                            <a href="http://csgobomj.ru/deposit" target="_blank" className="but_comr3 add-deposit ">Внести предметы</a>
                            </div>
                            <div className="clr" />
                        </div>
                        </div>
                        <div className="clr" />
                    </div>
                </div>
                <div className="dlock_link">
                    <a href="https://steamcommunity.com/id/user/edit/#personaName" target="_blank">Снизь комиссию на <b>3%</b>!<br />Добавь в ник <b>CSGOBOMJ.RU</b></a>
                    <a href="javascript:void();" data-tab={0}>Минимально <b>1 <span className="coins" /></b><br />Максимум - <b>10 предметов</b></a>
                    <a href="javascript:void();" data-tab={1} style={{display: 'none'}}>Максимально <b>100 <span className="coins" /></b><br />Максимум - <b>10 предметов</b></a>
                    <a href="javascript:void();">Ты первый?<br />Скидка <b>2%</b> на комиссию!</a>
                </div>
                <div className="clr" />
                <div id="linkBlock" className="link_obmen" style={{display: 'none'}}>
                    <h3>Укажите вашу ссылку на обмен</h3>
                    <form>
                        <input type="text" className="inp_link_obmen save-trade-link-input" placeholder="Ссылка на обмен..." />
                        <span className="save-trade-link-input-btn sub_but_obmen" />
                    </form>
                    <a href="http://steamcommunity.com/id/me/tradeoffers/privacy#trade_offer_access_url" target="_blank" className="link_obmen_wh">Где взять ссылку?</a>
                    <div className="clr" />
                </div>
                <div className="clr" />
            </Fragment>
        )
    }
}