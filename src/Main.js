import React, { Component, Fragment } from 'react';
import { Switch, Route, Link } from 'react-router-dom';

import { Roulette } from './Roulette';
import { About } from './About';
import { Top } from './Top';
import { GamesHistory } from './History';
import { Fair } from './Fair';
import { Referral } from './Referral';
import { Settings } from './Settings';
import { loadData } from './helpers/loadData';

import openSocket from 'socket.io-client';
import { Support } from './Support';
import { Shop } from './Shop';
import { Ticket } from './Support/ticket';
import { Profile } from './Profile';
const socket = openSocket(':7020', {
    transports: ['websocket']
});

export class Main extends Component {
    constructor(props) {
        super(props);
        const lastGame = JSON.parse(localStorage.getItem('lastGame'));

        this.state = {
            lastGame: lastGame === null ? {
                winner: {},
                price: 0
            } : lastGame,
            user: JSON.parse(localStorage.getItem('user')),
            online: 0
        }
        this.loadLastWinner();
        this.getUser();
    }

    getUser() {
        loadData('/getUser')
        .then(data => {
            const { user } = data;
            this.setState({
                user
            });
            localStorage.setItem('user', JSON.stringify(user));
        })
    }

    componentDidMount() {
        socket.on('online', online => {
            this.setState({
                online: Math.ceil(online / 6)
            })
        });
        socket.on('newGame', this.loadLastWinner.bind(this));
    }
    
    loadLastWinner() {
        loadData('/getLastWinner')
          .then(data => {
              const { lastGame } = data;
              this.setState({
                  lastGame: lastGame === null ? {
                      winner: {},
                      price: 0
                  } : lastGame
              });
              localStorage.setItem('lastGame', JSON.stringify(lastGame));
          })
    }

    render() {
        const { lastGame, online, user } = this.state;

        return (
            <Switch>
                <Route exact path='/' render={props => <Roulette key="index" openProfile={this.props.openProfile} historyGame={false} {...props} />} />
                <Route path='/game/:gameId' render={props => <Roulette key={`historyGame_${props.match.params.gameId}`} openProfile={this.props.openProfile} historyGame={true} {...props} />} />
                <Route path='/profile/:steamid64' component={Profile} />
                <Route path='/history' component={GamesHistory} />
                <Route path='/top' component={Top} />
                <Route path='/fair' component={Fair} />
            </Switch>
        )

        // return (
        //     <main role="main" className="content">
        //         <div className="container">
        //             <div className="row">
        //             <div className="col-lg-4  mb-4 aside mx-auto mb-3 order-2 order-lg-1">
        //                 <nav className="nav flex-column mb-3 hidden-md-down">
        //                     <Link className="nav-link" to="/"><span>Играть</span></Link>
        //                     <Link className="nav-link link__1" to="/about"><span>О нас</span></Link>
        //                     <Link className="nav-link link__2" to="/top"><span>Топ игроков</span></Link>
        //                     <Link className="nav-link link__3" to="/history"><span>История игр</span></Link>
        //                     <Link className="nav-link link__4" to="/fair"><span>Честная игра</span></Link>
        //                     {user === null ? '' : <Link className="nav-link link__5" to="/settings"><span>Настройки</span></Link>}
        //                     <Link className="nav-link link__6" to="/shop"><span>Магазин</span></Link>
        //                     <Link className="nav-link link__7" to="/referral"><span>Рефералка</span></Link>
        //                     <Link className="nav-link link__8" to="/support"><span>Поддержка</span></Link>
        //                     {/* <Link className="nav-link link__9" to="/news"><span>Статьи</span></Link> */}
        //                 </nav>
        //                 {/* <div className="card">
        //                 <div className="card-header">
        //                     <img src="/assets/img/bonus.png" alt />
        //                 </div>
        //                 <div className="card-body">
        //                     <p className="card-text" style={{textAlign: 'center'}}>Add to your Steam nickname <strong>PUBGWTF.COM</strong> and receive <strong>+5%</strong> to the winning items!</p>
        //                     <a href="http://steamcommunity.com/id/xx/edit" rel="nofollow" target="_blank" className="card-link">Details</a>
        //                 </div>
        //                 </div> */}
        //                 <div className="card">
        //                 <div className="card-header">
        //                     Последний победитель
        //                 </div>
        //                 <div className="card-body">
        //                     <div class="name-link">
        //                         <noindex>
        //                             <a href={`http://steamcommunity.com/profiles/${lastGame.winner.steamid64}`} target="_blank" rel="nofollow" class="avatar">
        //                                 <img src={lastGame.winner.avatar} className="border name-link mr-2" style={{width: '100px', height: '100px'}} alt />
        //                             </a>
        //                         </noindex>
        //                         <span>{lastGame.winner.username}</span>
        //                     </div>
        //                 </div>
        //                 <div className="card-footer">
        //                     <div className="d-inline-block flex-nowrap">
        //                     <div className="row">
        //                         <div className="col-6">Банк:</div>
        //                         <div className="col-6">{lastGame.price.toFixed(2)}<span class="flaticon-coins"></span></div>
        //                     </div>
        //                     <div className="row">
        //                         <div className="col-6">Шанс:</div>
        //                         <div className="col-6">{lastGame.winnerChance}%</div>
        //                     </div>
        //                     </div>
        //                 </div>
        //                 </div>
        //                 <div className="card pt-1 pb-1">
        //                 Онлайн {online}
        //                 </div>
        //             </div>
        //             <div className="col-lg-8 mt-3 mt-lg-0  main-right pl-lg-0 order-1  order-lg-2">
        //                 <Switch>
        //                     <Route exact path='/' render={props => <Roulette key="index" openProfile={this.props.openProfile} historyGame={false} {...props} />}/>
        //                     <Route path='/game/:gameId' render={props => <Roulette key={`historyGame_${props.match.params.gameId}`} openProfile={this.props.openProfile} historyGame={true} {...props} />} />
        //                     <Route path='/about' component={About} />
        //                     <Route exact path='/support' component={Support} />
        //                     <Route path='/support/:ticketId' component={Ticket} />
        //                     <Route path='/shop' component={Shop} />
        //                     {/* <Route path='/refferal' component={About} /> */}
        //                     <Route path='/news' component={About} />
        //                     <Route path='/top' component={Top} />
        //                     <Route path='/history' component={GamesHistory} />
        //                     <Route path='/fair' component={Fair} />
        //                     <Route path='/referral' component={Referral} />
        //                     <Route path='/settings' component={Settings} />
        //                 </Switch>
        //             </div>
        //             </div>
        //             <div className="border-line" />   
        //         </div>
        //     </main>
        // )
    }
}