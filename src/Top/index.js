import React, { Component } from 'react';
import { loadData } from '../helpers/loadData';

export class Top extends Component {
    constructor(props) {
        super(props);
        const users = JSON.parse(localStorage.getItem('usersTop'));
        const period = localStorage.getItem('topPeriod');

        this.state = {
            users: users === null ? [] : users,
            period: period === null ? 'day' : period
        };
        this.loadTop(this.state.period);
    }

    loadTop(period) {
        loadData(`/getTop/${period}`)
          .then(data => {
              const { users } = data;
              this.setState({
                  users
              });
              localStorage.setItem('usersTop', JSON.stringify(users));
          })
    }

    changePeriod(period) {
        this.setState({
            period
        });
        localStorage.setItem('topPeriod', period);
        this.loadTop(period);
    }

    isPeriodActive(period) {
        return this.state.period == period ? 'active' : '';
    }

    render() {
        const { users } = this.state;
        return (
            <div className="block_history">
                <div className="title_block_all"><span>ТОП 20 ПОБЕДИТЕЛЕЙ</span></div>
                <div className="clr" />
                <table className="top20">
                    <thead>
                    <tr>
                        <td>Место</td>
                        <td>Профиль</td>
                        <td>Участий</td>
                        <td>Побед</td>
                        <td>WIN-RATE</td>
                        <td>Сумма банков</td>
                    </tr>
                    </thead>
                    <tbody>
                        {users.map((user, index) => (
                            <tr>
                                <td className="winner-count">
                                    <span className="rate_top20">{index + 1}</span>
                                </td>
                                <td className="winner-name">
                                    <a href="/user/Ntbk8ckJtV"><img src={user.avatar} />{user.username}</a>
                                </td>
                                <td className="participations">{user.joins}</td>
                                <td className="win-count">{user.wins}</td>
                                <td className="winrate"><span className="win_rate_top20">{user.winRate}%</span></td>
                                <td className="round-sum"><span className="summa_banka_top20">{user.bank}</span></td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        )
    }
}