import React, { Component, Fragment } from 'react';
import sha256 from 'sha256';

export class Fair extends Component {
    constructor(props) {
        super(props);

        this.state = {
            ticketsCount: '',
            hash: '',
            secret: '',
            secret_word: '',
            winnerTicket: ''
        }
    }

    checkFair(event) {
        event.preventDefault();
        const {
            ticketsCount,
            secret,
            secret_word,
            hash
        } = this.state;
        const expectedHash = sha256(secret.toString() + secret_word.toString());
        const hashCorrect = expectedHash == hash;
        const winnerTicket = Math.ceil(ticketsCount * secret);
        this.setState({
            winnerTicket: `Выигрышный билет - ${winnerTicket}, хэш ${hashCorrect ? 'совпадает' : "не совпадает"}`
        });
    }

    render() {
        return (
            <Fragment>
                <div className="block_history">
                    <div className="title_block_all"><span>ЧЕСТНАЯ ИГРА</span></div>
                    <div className="clr" />
                    <div className="full_content">
                        <h3>Как это работает?</h3>
                        <p>Наша система честной игры работает таким образом, что победитель определяется с помощью Числа раунда, которое случайным образом генерируется в начале игры.</p>
                        <p>Число раунда зашифровывается с помощью MD5, и этот хэш показывается в начале каждого раунда.</p>
                        <p>Хэш также содержит рандомную строку (Секрет), чтобы предотвратить расшифровку.</p>
                        <p>В конце раунда система показывает то самое расшифрованное Число раунда и Секрет, которые были зашифрованы в самом начале, и вы сможете проверить, что Число раунда не менялось на протяжении игры.</p>
                        <p>Число раунда умножается на общее количество билетов в раунде и таким образом выбирается победный билет. У кого из игроков будет данный победный билет, тот и окажется победителем. </p>
                        <p>То есть принцип честной игры работает таким образом, что мы никак не можем знать сколько билетов будет на момент завершения раунда, а Число раунда для умножения дается в самом начале раунда.</p>
                        <h3>ОБОЗНАЧЕНИЯ</h3>
                        <p><span>ЧИСЛО РАУНДА</span></p>
                        <p>Случайное дробное число от 0 до 1 (например: 0.8612523461234567)</p>
                        <p><span>ХЭШ</span></p>
                        <p>MD5 хэш шифруется из комбинации секрета и числа раунда, используется чтобы доказать честность игры.</p>
                        <p><span>СЕКРЕТ</span></p>
                        <p>Случайная строка с учетом регистра используется, чтобы предотвратить расшифровку хэша.</p>
                        <p><span>БИЛЕТ</span></p>
                        <p>За каждые внесенные 0.10<span className="coins" /> вы получите 1 билет (10<span className="coins" /> = 100 билетов).</p>
                        <h3>ВЫБОР ПОБЕДИТЕЛЯ</h3>
                        <p>Каждый депозит переводится в билеты. Билеты сортируются по времени депозита.</p>
                        <p>Номер победного билета считается по следующей формуле: <span>floor(число билетов * число раунда) + 1 = победитель</span></p>
                        <p>(функция floor возвращает ближайшее целое число, округляя переданное ей число в меньшую сторону). </p>
                        <p>Игрок, у которого будет выбранный победный билет и окажется победителем в раунде.</p>
                        <h3>ПРОВЕРКА</h3>
                        <p>Вы можете использовать этот инструмент, чтобы убедиться, что хэш соответствует Секрету и Числу раунда, и вычислить номер победного билета.</p>
                        <div id="check">
                        <div className="honest-form">
                            <input value={this.state.ticketsCount} onChange={event => this.setState({ticketsCount: event.target.value})} id="totalbank" type="text" placeholder="Число билетов" />
                            <input value={this.state.secret} onChange={event => this.setState({secret: event.target.value})} id="roundRandom" type="text" placeholder="Число раунда" />
                            <input value={this.state.secret_word} onChange={event => this.setState({secret_word: event.target.value})} id="roundSecret" type="text" placeholder="Секрет" />
                            <input value={this.state.hash} onChange={event => this.setState({hash: event.target.value})} id="roundHash" type="text" placeholder="Хэш" />
                        </div>
                        <div className="clr" />
                        <div id="checkHash" onClick={this.checkFair.bind(this)} className="honest-check">Проверить</div>
                        <div id="checkResult" className="honest-check-text">{this.state.winnerTicket} &nbsp; </div>
                        </div>
                    </div>
                </div>
                <div className="clr" />
            </Fragment>
        )
    }
}