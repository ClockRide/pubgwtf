import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { loadData } from './helpers/loadData';

export class Header extends Component {
    constructor(props) {
        super(props);

        this.state = {
            navbarCollapsed: true,
            user: JSON.parse(localStorage.getItem('user'))
        }
    }

    getUser() {
        loadData('/getUser')
        .then(data => {
            const { user } = data;
            this.setState({
                user
            });
            localStorage.setItem('user', JSON.stringify(user));
        })
    }

    toggleCollapse(event) {
        this.setState(prevState => {
            prevState.navbarCollapsed = !prevState.navbarCollapsed;
            return prevState;
        });
    }

    render() {
        const { user } = this.state;

        return (
            <div className="header">
                <Link to="/" className="logo">CSGO<span>BOMJ</span>.RU</Link>
                <ul>
                    <li><Link to="/history">ИСТОРИЯ ИГР</Link></li>
                    <li><Link to="/top">ТОП</Link></li>
                    <li><Link to="/fairplay">ЧЕСТНАЯ ИГРА</Link></li>
                </ul>
            </div>
        )
    }
}