import React, { Component } from 'react';
import { Footer } from './Footer';
import { Header } from './Header';
import { Main } from './Main';
import { Chat } from './Chat';
import { ProfileModal } from './ProfileModal';
import { activateCode } from './Referral/activateCode';
import { ContentHeader } from './ContentHeader';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
        profileOpened: false,
        profileSteamid64: 0
    };
    const refCode = this.findGetParameter('r');
    if (refCode !== null) {
      activateCode(refCode, undefined, undefined, data => document.location = '/');
    }
  }

  findGetParameter(parameterName) {
    let result = null,
        tmp = [];
    window.location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
          tmp = item.split("=");
          if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}

  openProfile(steamid64) {
      this.setState({
          profileOpened: true,
          profileSteamid64: steamid64
      });
  }

  closeProfile() {
      this.setState({
          profileOpened: false
      });
  }

  render() {
    return (
        <div className="content">
            <Header />
            <div className="clr" />
            <div className="content_white">
                <ContentHeader />
                <Main />
            </div>
            <div className="clr" />
        </div>
    );
  }
}

export default App;