import React, { Component } from 'react';
import { loadData } from '../helpers/loadData';
import { getSumOfProperty } from '../helpers/sumOfProperty';
import { ToastContainer, toast } from 'react-toastify';
import ReactPaginate from 'react-paginate';

export class Shop extends Component {
    constructor(props) {
        super(props);

        const items = JSON.parse(localStorage.getItem('shopItems'));
        const cartItems = JSON.parse(localStorage.getItem('cartItems'));
        const user = JSON.parse(localStorage.getItem('user'));

        this.state = {
            page: 1,
            user: user === null ? {} : user,
            shop: items === null ? {
                last_page: 1,
                data: []
            } : items,
            cart: cartItems === null ? [] : cartItems
        };
        this.loadItems();
        this.loadUser();
    }

    changePage(page) {
        page = page.selected + 1;
        this.setState({ page });
        this.loadItems(page);
    }

    addToCart(itemId, event) {
        event.preventDefault();
        this.setState(prevState => {
            const itemIndex = prevState.shop.data.findIndex(item => item.id == itemId);
            prevState.cart.push(prevState.shop.data[itemIndex]);
            prevState.shop.data.splice(itemIndex, 1);
            return prevState;
        });
    }

    removeFromCart(itemId, event) {
        if (typeof event !== "undefined")
            event.preventDefault();
        this.setState(prevState => {
            const itemIndex = prevState.cart.findIndex(item => item.id == itemId);
            prevState.shop.data.push(prevState.cart[itemIndex]);
            prevState.cart.splice(itemIndex, 1);
            return prevState;
        });
    }

    flushCart() {
        this.setState({cart: []});
    }

    refreshShop() {
        this.loadItems();
    }

    loadItems(page) {
        loadData(`/shop/getItems?page=${typeof page !== "undefined" ? page : this.state.page}`)
        .then(data => {
            const { items } = data;
            localStorage.setItem('shopItems', JSON.stringify(items));
            this.setState({
                shop: items,
                cart: []
            });
        })
    }

    loadUser() {
        loadData('/getUser')
        .then(data => {
            const { user } = data;

            this.setState({
                user
            });
        })
    }

    proceedPay(event) {
        event.preventDefault();
        
        loadData('/shop/proceedPay', {cart: this.state.cart.map(item => item.id)})
        .then(data => {
            if (data.success) this.refreshShop();
            toast(data.text, {
                type: data.success ? toast.TYPE.SUCCESS : toast.TYPE.ERROR
            });
        })
    }

    render() {
        const { shop, cart, user } = this.state;
        return (
            <div>
                <ToastContainer />
                <div className="title-row d-flex justify-content-center mb-2">
                    Магазин
                </div>
                <div className="text-uppercase">Корзина</div>
                <div className="d-flex flex-wrap flex-md-nowrap">
                    <div className="slide-line slide-roll mb-3 mb-md-0">
                        <ul className="nav flex-nowrap justify-content-start">
                        {cart.map(item => (
                            <li key={item.id} className="shopItem" style={{marginTop: '0px'}}>
                                <a href="#" style={{borderTop: `${item.color} solid 5px`, paddingTop: '0px'}} onClick={this.removeFromCart.bind(this, item.id)}>
                                    <div className="itemsImages-block">
                                        <img src={item.image} className="itemsImages" />
                                    </div>
                                    <div className="caption-top-block">
                                        <h1 className="caption caption-top">{item.name}</h1>
                                    </div>
                                    <span className="caption caption-bot">{parseFloat(item.shop_price).toFixed(2)}
                                        <i className="flaticon-coins" />
                                    </span>
                                </a>
                            </li>
                        ))}
                        </ul>
                    </div>
                    <div className="card basket mx-auto ml-md-1">
                        <div>Баланс: <strong>{user === null ? '' : parseFloat(user.money).toFixed(2)}</strong><i className="flaticon-coins color" /></div>
                        <div>Цена корзины: <strong>{getSumOfProperty(cart, 'shop_price').toFixed(2)}</strong><i className="flaticon-coins color" /></div>
                        <div className="d-flex justify-content-around mt-2 line-btn">
                            <a href="#" className="btn danger" onClick={this.flushCart.bind(this)}>
                                <i className="flaticon-commerce" />
                            </a>
                            <a href="#" className="btn" onClick={this.refreshShop.bind(this)}>
                                <i className="flaticon-exchange" />
                            </a>
                            <a href="#" onClick={this.proceedPay.bind(this)} className="btn success ">
                                <i className="flaticon-add-to-cart" />
                            </a>
                        </div>
                    </div>
                </div>
                <hr />
                <div>
                    <div className="shop">
                        <ul className="itemList nav">
                        {shop.data.map(item => (
                            <li className="shopItem" key={item.id} style={{borderBottomColor: item.color}}>
                                <a href="#" style={{borderTop: `${item.color} solid 5px`, paddingTop: '0px'}} onClick={this.addToCart.bind(this, item.id)}>
                                    <div className="itemsImages-block">
                                        <img className="itemsImages" src={item.image} />    
                                    </div>
                                    <div className="caption-top-block">
                                        <h1 className="caption caption-top">{item.name} <br /> {/*item.rarity.replace('#', '')*/}</h1>
                                    </div>
                                    <span className="caption caption-bot"> {parseFloat(item.shop_price).toFixed(2)} <i className="flaticon-coins" /></span>
                                </a>
                            </li>
                        ))}
                        </ul>
                        <ReactPaginate pageCount={shop.last_page} activeClassName="active" onPageChange={this.changePage.bind(this)} pageRangeDisplayed={5} pageLinkClassName="page-link" pageClassName="page-item" containerClassName="pagination justify-content-center" marginPagesDisplayed={2} />
                    </div>
                    {/* <nav aria-label="Page navigation example">
                        <ul className="pagination mt-1  justify-content-center">
                        <li className="page-item">
                            <a className="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true" className="flaticon-left-arrow" />
                            <span className="sr-only">Previous</span>
                            </a>
                        </li>
                        <li className="page-item"><a className="page-link active" href="#">1</a></li>
                        <li className="page-item"><a className="page-link" href="#">2</a></li>
                        <li className="page-item"><a className="page-link" href="#">3</a></li>
                        <li className="page-item"><a className="page-link" href="#">4</a></li>
                        <li className="page-item"><a className="page-link" href="#">5</a></li>
                        <li className="page-item">
                            <a className="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true" className="flaticon-arrows" />
                            <span className="sr-only">Next</span>
                            </a>
                        </li>
                        </ul>
                    </nav> */}
                    {/* <div className="card">
                        <div className="card-body p-3">
                        <div className="title text-center mb-3">Рулетка CS GO для бомжей с минимальной ставкой 1 рубль</div>
                        <p>Рулетки кс го от 1 рубля – это сервисы обмена виртуальными предметами между игроками Steam. Продолжительность новой игры длиться 120 секунд. Игра запускается только после того, как присоединяется второй участник. Честные рулетки кс го для бомжей, как правило, принимают ставки CS GO от 1 рубля. Максимально допустимое количество вещей для раунда 100 предметов. Чтобы сделать свои первые ставки CS GO скинами, пройдите авторизацию на нашем сайте через специальное приложение Steam и мобильный аунтефикатор.</p>
                        </div>
                    </div> */}
                </div>
            </div>
        )
    }
}