import React, { Component } from 'react';
import sha256 from 'sha256';

export class Fair extends Component {
    constructor(props) {
        super(props);

        this.state = {
            ticketsCount: '',
            hash: '',
            secret: '',
            secret_word: '',
            winnerTicket: ''
        }
    }

    checkFair(event) {
        event.preventDefault();
        const {
            ticketsCount,
            secret,
            secret_word,
            hash
        } = this.state;
        const expectedHash = sha256(secret.toString() + secret_word.toString());
        const hashCorrect = expectedHash == hash;
        const winnerTicket = Math.ceil(ticketsCount * secret);
        this.setState({
            winnerTicket: `Winning ticket - ${winnerTicket}, hash ${hashCorrect ? 'corresponds' : "doesn't corresponds"}`
        });
    }

    render() {
        return (
            <div className="card-block">
                <div class="title-row d-flex justify-content-center mb-3">Fair game</div>
                <div className="card">
                    <h5 className="card-title">HOW IT WORKS</h5>
                    <div className="card-body">
                    <p className="card-text">Our fair game system pick a winner using <span className="highlight">Round number</span>, which generated in the start of the game. <span className="highlight">Round number</span> hashed using SHA256 hash, and visible for everyone.</p>
                    <p className="card-text">In the end of the ruffle system shows <span className="highlight">Round number</span>, which was hashed in the start of game, and you can check, that <span className="highlight">Round number</span> didn't changed during the game. <span className="highlight">Round number</span> multiples to the total tickets count in the game and in that way website got winning ticket </p>
                    <p className="card-text">And owner of the winning ticket becomes winner. Simple and secure, isn't it?</p>
                    </div>
                </div>
                <div className="card">
                    <h5 className="card-title">Terms</h5>
                    <div className="card-body">
                    <p className="card-text">
                        <span className="text-title">Round number</span> Random float number from 0 to 1 (example: 0.349269263810378)
                    </p>
                    <p className="card-text">
                        <span className="text-title">Hash</span> SHA256 hashed from the round number, used to prove game fair.
                    </p>   
                    <p className="card-text">
                        <span className="text-title">Ticket</span> For each 1/100 part of coin you got 1 ticket (1 coin = 100 tickets).</p>
                    </div>
                </div>
                <div className="card">
                    <h5 className="card-title">Winner choise</h5>
                    <div className="card-body card-text__margin">
                    <p>Every bet converted to the tickets. Tickets sorted desc by deposit time.</p>
                    <p>Winning ticket choose by following formula: ceil(tickets in game count * round number) = winner (The ceil() function returns the largest integer greater than or equal to a given ).</p>
                    <p>Player with winning ticket is winner of the game</p>
                    </div>
                </div>
                <div className="card">
                    <h5 className="card-title">Check</h5>
                    <div className="card-body card-text__margin">
                    <p>
                        You can use following form to be sure that hash corresponds to round number and calculate winning ticket. 
                        Also for checking SHA256 hash you can such sites as 
                        <ul>
                            <li>
                                <a href="http://www.xorbin.com/tools/sha256-hash-calculator" target="_blank" rel="nofollow">xorbin.com</a>
                            </li>
                            <li>
                                <a href="https://passwordsgenerator.net/sha256-hash-generator/" target="_blank" rel="nofollow">passwordsgenerator.net</a>
                            </li>
                            <li>
                                <a href="https://hash.online-convert.com/ru/sha256-generator" target="_blank" rel="nofollow">hash.online-convert.com</a>
                            </li>
                            <li>
                                <a href="http://www.md5calc.com/sha256" target="_blank" rel="nofollow">md5calc.com</a>
                            </li>
                            <li>
                                <a href="http://www.convertstring.com/ru/Hash/SHA256" target="_blank" rel="nofollow">convertstring.com</a>
                            </li>
                        </ul>
                    </p>
                    <form onSubmit={this.checkFair.bind(this)}>
                        <div className="form-group">
                        <input value={this.state.ticketsCount} onChange={event => this.setState({ticketsCount: event.target.value})} type="text" className="form-control" placeholder="Tickets count" />
                        </div>
                        <div className="form-group">
                        <input value={this.state.secret} onChange={event => this.setState({secret: event.target.value})} type="text" className="form-control" placeholder="Round number" />
                        </div>
                        <div className="form-group">
                        <input value={this.state.hash} onChange={event => this.setState({hash: event.target.value})} type="text" className="form-control" placeholder="Hash" />
                        </div>
                        <div className="row  no-gutters">
                        <div className="col-sm-4 col-md-3 col-lg-2">
                            <div className="form-group">
                            <button type="submit" className="btn btn-block btn-sm btn-default btn-left">Проверить</button>
                            </div>
                        </div>
                        <div className="col-sm-8  col-md-9 col-lg-10 ">
                            <div className="form-group">
                            <input type="text" className="form-control test-result" readOnly value={this.state.winnerTicket} />
                            </div>
                        </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        )
    }
}