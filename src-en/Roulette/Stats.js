import React, { Component } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import { getFromLocalStorage } from '../helpers/getFromLocalStorage';
import { setToLocalStorage } from '../helpers/setToLocalStorage';
import { loadData } from '../helpers/loadData.js';

import openSocket from 'socket.io-client';

export class Stats extends Component {
    constructor(props) {
        super(props);

        const {
            uniqGamePlayers,
            gamesToday,
            maxBankToday,
            itemsRaffledToday
        } = getFromLocalStorage(['uniqGamePlayers', 'gamesToday', 'maxBankToday', 'itemsRaffledToday']);
        this.state = {
            uniqGamePlayers,
            gamesToday,
            maxBankToday,
            itemsRaffledToday
        };
        this.loadStats();
    }

    componentWillUnmount() {
        ['newGame'].forEach(item => this.socket.off(item));
    }

    componentDidMount() {
        this.socket = openSocket(':7020', {
            transports: ['websocket']
        });
        this.socket.on('newGame', this.loadStats.bind(this));
    }

    loadStats() {
        loadData('/getStats')
        .then(data => {
            this.setState(data);
            setToLocalStorage(data);
        })
        .catch(err => {
            toast('Stats loading error', {
                type: toast.TYPE.ERROR 
            });
        });
    }

    render() {
        const {
            uniqGamePlayers,
            gamesToday,
            maxBankToday,
            itemsRaffledToday
        } = this.state;
        
        return (
            <div className="row promo">
                <ToastContainer />
                <div className="col-sm-6 col-lg-3">
                <div className="card">
                    <div className="card-header card-title__lg">{gamesToday}</div>
                    <div className="card-body">Games
                    <br /> today</div>
                </div>
                </div>
                <div className="col-sm-6 col-lg-3">
                <div className="card">
                    <div className="card-header card-title__lg">{uniqGamePlayers}</div>
                    <div className="card-body">Unique players
                    <br /> today</div>
                </div>
                </div>
                <div className="col-sm-6 col-lg-3">
                <div className="card">
                    <div className="card-header card-title__lg">{itemsRaffledToday}</div>
                    <div className="card-body">Items ruffled
                    <br /> today</div>
                </div>
                </div>
                <div className="col-sm-6 col-lg-3">
                <div className="card">
                    <div className="card-header card-title__lg">{maxBankToday}</div>
                    <div className="card-body">Max bank
                    <br /> today
                    </div>
                </div>
                </div>
            </div>
        )
    }
}