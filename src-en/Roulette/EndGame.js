import React, { Component } from 'react';
import openSocket from 'socket.io-client';
import { GameConfig } from '../config/GameConfig';
import { GameStates } from '../config/GameStates';
import { getItemImage } from '../helpers/getItemImage';
import { Slider } from './Slider';
import { BotLink } from '../config/BotLink';

const socket = openSocket(':7020', {
    transports: ['websocket']
});

export class EndGame extends Component {
    constructor(props) {
        super(props);

        this.state = {
            timer: this.props.historyGame ? 0 : GameConfig.prefinishedTimer
        }
    }

    formatNumber(number) {
        number = number.toString();
        if (number.length === 0) return '00';
        if (number.length === 1) return '0' + number;
        return number; 
    }

    getMinutes() {
        return this.formatNumber(Math.floor(this.state.timer / 60));
    }
    
    getSeconds() {
        return this.formatNumber(this.state.timer - this.getMinutes() * 60);
    }

    componentDidMount() {
        socket.on('timer', this.handleTimerChange.bind(this))
    }

    getWinnerItem() {
        const { winnerBet } = this.props.game;
        const items = JSON.parse(winnerBet.items);
        return getItemImage(items[0].classid);
    }

    handleTimerChange(timer) {
        if (this.props.historyGame) return;
        this.setState({
            timer
        })
    }

    render() {
        const { timer } = this.state;
        const { game, historyGame } = this.props;
        const resultsVisible = timer < 10 || historyGame;
        return (
            <div class="card">
                <Slider game={game} historyGame={historyGame} resultsVisible={resultsVisible} winningItem={this.getWinnerItem()} />
                <div className="card-body">
                    <div className="row row-start align-content-center   align-items-stretch info-panel flex-md-wrap flex-wrap ">
                        <div className="panel-left col-md-6 col-xl-4  align-items-stretch d-flex   flex-wrap">
                        <p class="nowrap">Winning ticket: <span className="highlight">#{resultsVisible ? game.winner_ticket : '???????'}</span></p>
                        <p class="nowrap">Round number: <span className="highlight">{resultsVisible ? game.secret : '???????'}</span></p>
                        <p class="nowrap">Winner: <span className="highlight">{resultsVisible ? game.winner.username : '???????'}</span></p>
                        <span className="badge-text btn"> {game.price}<i className="flaticon-coins" /></span>
                        </div>
                        <div className="panel-right col-md-6 col-12  offset-xl-4 col-xl-4 d-flex flex-column">
                        <div className="d-flex justify-content-center justify-content-xl-between align-items-center pt-xl-0 pb-xl-0 pt-4 pb-4 ">
                            <span className="mr-4">New game after</span>
                            <div className="timer border">{this.getSeconds()}</div>
                        </div>
                        <a href={BotLink} target="_blank" className="btn btn-lg btn-sign btn-block btn-in mt-auto">Deposit items</a> </div>
                    </div>
                </div>
            </div>
        )
    }
}