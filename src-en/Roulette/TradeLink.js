import React, { Component } from 'react';
import { loadData } from '../helpers/loadData';
import { ToastContainer, toast } from 'react-toastify';

export class TradeLink extends Component {
    constructor(props) {
        super(props);

        const { trade_link } = this.props.user;
        this.state = {
            trade_link: trade_link === null ? 'Например: https://steamcommunity.com/tradeoffer/new/?partner=173575212&token=MwT0Vor' : trade_link
        }
    }

    changeLink() {
        loadData('/changeLink', {
            trade_link: this.state.trade_link
        })
        .then(data => {
            if (data.success && typeof this.props.hideLinkInput !== "undefined") this.props.hideLinkInput();
            return toast(data.text, {
                type: data.success ? toast.TYPE.SUCCESS : toast.TYPE.ERROR
            });
        })
    }

    render() {
        const { user } = this.props;
        return (
            <div className="card card__succes">
                <ToastContainer />
                <div className="card-body ">
                <div className="text-left mb-1">Input your trade link. <a rel="nofollow" href="http://steamcommunity.com/id/me/tradeoffers/privacy" target="_blank" className="link-decorated">(Where is it?</a>)</div>
                <form action className="mb-1 form-small">
                    <div className="input-group form-group group-large">
                    <input
                        value={this.state.trade_link}
                        onChange={event => this.setState({
                            trade_link: event.target.value
                        })}
                        onClick={event => this.setState({
                            trade_link: ''
                        })}
                        type="text" 
                        className="form-control"
                        aria-label="Example: https://steamcommunity.com/tradeoffer/new/?partner=173575212&token=MwT0Vor" 
                        aria-describedby="basic-addon2" 
                    />
                    <div className="input-group-prepend">
                        <a onClick={this.changeLink.bind(this)} href="#" className="btn  btn-dark  btn-block  btn-addon" id="basic-addon2">Save link</a>
                    </div>
                    </div>
                </form>
                <div className="text-left">Open your <a rel="nofollow" href={`http://steamcommunity.com/profiles/${user.steamid64}/edit/settings/`} target="_blank" className="link-decorated">Steam inventory</a></div>
                </div>
            </div>
        )
    }
}