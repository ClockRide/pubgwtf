import React, { Component } from 'react';
import { Timer } from './Timer';
import { GameConfig } from '../config/GameConfig';
// import { BotLink } from '../config/BotLink';
import { ToastContainer, toast } from 'react-toastify';

export class GameHeader extends Component {
    checkLink(event) {
        const { user } = this.props;
        if (user.access_token === null) {
            toast('Please input your trade link', {
                type: toast.TYPE.ERROR
            });
            event.preventDefault();
        }
    }

    render() {
        const { game, user } = this.props;
        let userChance = 0;
        if (user !== null && game.price > 0) {
            const userBetsSum = game.bets.filter(bet => bet.user_id == user.id).map(bet => bet.price).reduce((prev, current) => prev + parseFloat(current), 0);
            userChance = userBetsSum / (game.price / 100);
        }
        return (
            <div className="card">
                <ToastContainer />
                <div className="title-center">
                <span>GAME</span>
                </div>
                <div className="card-body">
                <div className="row no-gutters align-items-center  flex-wrap flex-lg-nowrap progress-block mb-4">
                    <div className="col-lg-8">
                    <div className="progress">
                        <div className="progress-bar progress-bar-striped" role="progressbar" style={{width: `${game.items.length / (GameConfig.itemsCount / 100)}%`}}>
                        </div>
                        <span className="progress-text"><span>{game.items.length}/{GameConfig.itemsCount}</span> items</span>
                    </div>
                    </div>
                    <span className="col-12 col-lg-1">or</span>
                    <Timer />
                </div>
                <div className="titile-go d-flex align-items-center justify-content-center mx-auto titile-go__small  mb-4">
                    <div className="d-flex align-items-center">{user === null ? 'Join game and win' : 'Bank'}&nbsp;<span>{parseFloat(game.price).toFixed(2)}</span><span className="flaticon-coins" /></div>
                </div>
                {user === null ? (
                    <a href="/login" className="btn btn-lg btn-sign long-lg btn-block mx-auto mb-2"><span>Sign in</span></a>
                ) : (
                    <div className="row align-content-center   align-items-stretch info-panel flex-md-wrap flex-wrap ">
                    <div className="panel-left col-sm-6 col-xl-5  align-items-stretch d-flex   flex-wrap mb-2">
                        <p>You deposited {user === null ? 0 : game.bets.filter(bet => bet.user_id == user.id).map(bet => bet.itemsCount).reduce((prev, curr) => prev + parseInt(curr), 0)} (from {GameConfig.maxItemsTrade}) items</p>
                        <p><span className="highlight small">Min bet — 1 item. Max items in trade — {GameConfig.maxItemsTrade}, you can bet only once in game</span></p>
                        <p><span className="highlight small">Higher bet — higher chance.</span></p>
                    </div>
                    <div className=" col-sm-6 col-xl-3 d-flex   flex-wrap align-items-center mb-2">
                        <div className="chance d-inline-block">
                        Your chance
                        <div>{userChance.toFixed(2)}%</div>
                        </div>
                    </div>
                    <div className="panel-right col-12 col-xl-4 d-flex align-items-center  mb-2">
                        <a href={game.botLink} onClick={this.checkLink.bind(this)} target="_blank" className="btn btn-lg btn-sign btn-block btn-in  mt-0 ">Deposit items</a> </div>
                    </div>
                )}
                </div>
            </div>
        )
    }
}