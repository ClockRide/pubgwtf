import React, { Component } from 'react';
import { ChatMessage } from './ChatMessage';
import { loadData } from './helpers/loadData';
import { ToastContainer, toast } from 'react-toastify';

import openSocket from 'socket.io-client';

export class Chat extends Component {
    constructor(props) {
        super(props);
        const isAdmin = localStorage.getItem('isAdmin');

        this.state = {
            chatOpened: false,
            messages: [],
            message: '',
            isAdmin: isAdmin == 1
        };
        this.loadChat();
        this.loadIsAdmin();
    }

    loadIsAdmin() {
        loadData('/checkIsAdmin')
            .then(data => {
                const { isAdmin } = data;
                localStorage.setItem('isAdmin', isAdmin ? 1 : 0);
                this.setState({
                    isAdmin
                });
            })
            .catch(err => {
                console.error('Error with checking previleges');
            });
    }

    deleteMessage(index) {
        this.setState(prevState => {
            prevState.messages.splice(index, 1);
            return prevState;
        });
    }

    componentWillUnmount() {
        ['newMessage'].forEach(item => this.socket.off(item));
    }

    componentDidMount() {
        this.socket = openSocket(':7020', {
            transports: ['websocket']
        });
        setTimeout(() => this.messages.scrollIntoView(false), 200);
        this.socket.on('newMessage', message => {
            message = JSON.parse(message);
            this.setState(prevState => {
                prevState.messages.push(message);
                if (prevState.messages.length > 30) prevState.messages.splice(0, 1);
                setTimeout(() => this.messages.scrollIntoView(false), 200);
                return prevState;
            });
        });
        
    }

    loadChat() {
        loadData('/getChat')
            .then(data => {
                const { messages } = data;
                this.setState({
                    messages
                });
                setTimeout(() => this.messages.scrollIntoView(false), 200);
            })
            .catch(err => {
                toast('Error with chat loading', {
                    type: toast.TYPE.ERROR
                });
            })
    }

    sendMessage(event) {
        event.preventDefault();

        const { message } = this.state;

        loadData('/sendMessage', {
            message
        })
            .then(data => {
                if (data.success) {
                    this.setState({
                        message: ''
                    });
                } else {
                    toast(data.text, {
                        type: toast.TYPE.ERROR
                    });
                }
            })
            .catch(err => {
                toast('Error with chat loading', {
                    type: toast.TYPE.ERROR
                });
            })
    }

    toggleVisible() {
        this.setState(prevState => {
            prevState.chatOpened = !prevState.chatOpened;
            if (prevState.chatOpened)
                setTimeout(() => this.messages.scrollIntoView(false), 200);
            return prevState;
        });
    }

    handleKeyPress(event) {
        if (event.key === 'Enter') this.sendMessage(event);
    }

    render() {
        const { messages, chatOpened, isAdmin } = this.state;

        return (
            <div className="chat">
            <ToastContainer />
            <div className="title-row border-bottom-0 mb-2" onClick={this.toggleVisible.bind(this)}>
                <button style={{display: chatOpened ? 'block' : 'none'}} type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <span className="icon chat-icon" />ЧАТ
            </div>
            <div className="list-media" style={{display: chatOpened ? 'block' : 'none'}}>
                <ul className="list-unstyled" ref={element => this.messages = element}>
                    {messages.map((messageJson, i) => (
                        <ChatMessage deleteMessage={this.deleteMessage.bind(this, i)} isAdmin={isAdmin} messageJson={messageJson} />
                    ))}
                    
                </ul>
            </div>
            <form onSubmit={this.sendMessage.bind(this)} style={{display: chatOpened ? 'block' : 'none'}}>
                <div className="form-group form-border">
                <textarea className="form-control" id="exampleFormControlTextarea1" value={this.state.message} onKeyPress={this.handleKeyPress.bind(this)} placeholder="Текст сообщения..." rows={5} onChange={event => this.setState({message: event.target.value})}  />
                <button type="submit" className="btn btn-block">ОТПРАВИТЬ <span className="icon mail" /></button>
                </div>
            </form>
            </div>
        )
    }
}