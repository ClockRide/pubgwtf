import { GameConfig } from '../config/GameConfig';

export const getItemImage = classid => `https://steamcommunity-a.akamaihd.net/economy/image/class/${GameConfig.appID}/${classid}/101fx101f`;