export const loadData = (url, body) => {
    return fetch(url, {
        method: 'POST',
        credentials: 'same-origin',
        body: JSON.stringify(body)
    })
    .then(response => response.json());
}