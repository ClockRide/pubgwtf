import React, { Component } from 'react';

import { loadData } from '../helpers/loadData';
import { getItemImage } from '../helpers/getItemImage';

import ReactPaginate from 'react-paginate';
import moment from 'moment';
import { Link } from 'react-router-dom';

export class GamesHistory extends Component {
    constructor(props) {
        super(props);
        const historyGames = JSON.parse(localStorage.getItem('historyGames'));

        this.state = {
            games: historyGames === null ? {
                last_page: 1,
                data: []
            } : historyGames,
            page: 1
        };
        this.loadGames();
    }

    loadGames(page) {
        loadData(`/getGames?page=${typeof page !== "undefined" ? page : this.state.page}`)
          .then(data => {
              const { games } = data;
              this.setState({
                  games
              });
              localStorage.setItem('historyGames', JSON.stringify(games));
          })
    }

    getPages() {
      const { page, games } = this.state;
      let { last_page } = games;
      let pages = [];
      const pagesToDisplay = last_page - page;
      if (pagesToDisplay > 4) last_page = page + 4;
      
      for (let i = page; i <= last_page; i++) pages.push(i);
      return pages;
    }

    sortItems(a, b) {
        if (a.price > b.price) return -1;
        if (a.price < b.price) return 1;
        if (a.index > b.index) return -1;
        if (a.index < b.index) return 1;
        return 0;
    }

    changePage(page) {
        page = page.selected + 1;
        this.setState({ page });
        this.loadGames(page);
    }

    formatNumber(number) {
      number = number.toString();
      if (number.length === 0) return '00';
      if (number.length === 1) return '0' + number;
      return number; 
    }

    incrementPage(by, event) {
        event.preventDefault();
        this.setState(prevState => {
          if (prevState.page + by < 1 || prevState.page + by > prevState.games.to) return prevState;
          prevState.page += by;
          this.loadGames(prevState.page);
          return prevState;
        });
    }

    render() {
        const { games, page } = this.state;
        return (
            <div>
                <div class="title-row d-flex justify-content-center mb-3">
                    Games history
                </div>
                {games.data.map(game => {
                  const date = moment(game.updated_at);
                  const time = `${this.formatNumber(date.hours())}:${this.formatNumber(date.minutes())}`;
                  const fullDate = `${date.year()}-${this.formatNumber(date.month() + 1)}-${this.formatNumber(date.date())}`;
                  return (
                    <div className="card" style={{marginTop: '-7px'}}>
                    <div className="card-body pl-lg-5 pr-lg-5" style={{width: '100%'}}>
                      <div className="d-flex align-content-center justify-content-xl-between justify-content-center align-items-center info-panel flex-wrap ">
                        <div className="d-flex flex-wrap ">
                          <noindex>
                            <a href={`http://steamcommunity.com/profiles/${game.winner.steamid64}`} target="_blank" rel="nofollow" class="avatar">
                              <img style={{width: '60px', width: '60px'}} src={game.winner.avatar} className="img-fluid border mr-2" alt />
                            </a>
                          </noindex>
                          <div className="text-left">
                            {game.winner.username}
                            <div>Game: <span className="highlight">#{game.id}</span></div>
                            <div>Bank: <span className="highlight nowrap">{game.price.toFixed(2)} <span className="flaticon-coins" /></span>
                            </div>
                            <div>Chance: <span className="highlight">{game.winnerChance}%</span></div>
                          </div>
                        </div>
                        <div className="text-left align-self-end pl-4 pr-4">
                          <div>Time: {time}</div>
                          <div>Date: {fullDate}</div>
                        </div>
                        <div className=" align-self-end">
                          <div>
                            {game.tradeSentState == 1 ? (
                                <a href="#" onClick={event => event.preventDefault()} className="btn btn-border btn-success mb-2 ">Items sent</a>
                            ) : ''}
                            {game.tradeSentState == 0 ? (
                                <a href="#" onClick={event => event.preventDefault()} className="btn btn-border mb-2 ">Sending...</a>
                            ) : ''}
                            {game.tradeSentState == 2 ? (
                                <a href="#" onClick={event => event.preventDefault()} className="btn btn-border btn-danger mb-2 ">Error with items send</a>
                            ) : ''}
                          </div>
                          <Link to={`/game/${game.id}`} className="btn-link">Game details</Link>
                        </div>
                      </div>
                      <div className="top-border mt-3">Winner items:</div>
                      <div className="d-flex align-items-center ">
                        <div className="slide-line slide-win">
                          <ul className="nav flex-nowrap  justify-content-start">
                            {game.winnerItems
                            .map((item, i) => {
                                item.index = i;
                                return item;
                            })
                            .sort(this.sortItems)
                            .filter((item, i) => i < 5).map((item, i) => (
                                <li key={i}><img src={typeof item.image === "undefined" ? getItemImage(item.classid) : item.image} className="history-image"/></li>
                            ))}
                          </ul>
                        </div>
                        {game.winnerItems.length > 5 ? (
                            <div className="win-right ">
                            +{game.winnerItems.length - 5}
                          </div>
                        ) : ''}
                        
                      </div>
                      <div className="top-border ">Commission:</div>
                      <div className="d-flex align-items-center ">
                        <div className="slide-line slide-win">
                          <ul className="nav flex-nowrap  justify-content-start">
                            {game.commission
                            .map((item, i) => {
                              item.index = i;
                              return item;
                            })
                            .sort(this.sortItems)
                            .filter((item, i) => i < 5).map((item, i) => (
                                <li key={i}><img src={typeof item.image === "undefined" ? getItemImage(item.classid) : item.image} alt /></li>
                            ))}
                          </ul>
                        </div>
                        {game.commission.length > 5 ? (
                            <div className="win-right ">
                            +{game.commission.length - 5}
                          </div>
                        ) : ''}
                      </div>
                    </div>
                  </div>                  
                )
                })}
                <ReactPaginate pageCount={games.last_page} activeClassName="active" onPageChange={this.changePage.bind(this)} pageRangeDisplayed={5} pageLinkClassName="page-link" pageClassName="page-item" containerClassName="pagination justify-content-center" marginPagesDisplayed={2} />
                {/* <nav aria-label="Page navigation example">
                    <ul class="pagination  justify-content-center">
                        <li class="page-item">
                            <a class="page-link" href="#" onClick={this.incrementPage.bind(this, -1)} aria-label="Previous">
                              <span aria-hidden="true" class="flaticon-left-arrow"></span>
                              <span class="sr-only">Previous</span>
                            </a>
                        </li>
                        {this.getPages().map(currentPage => <li class="page-item"><a class={`page-link ${currentPage == page ? 'active' : '' }`} onClick={this.changePage.bind(this, currentPage)} href="#">{currentPage}</a></li>)}
                        <li class="page-item">
                            <a class="page-link" href="#" onClick={this.incrementPage.bind(this, +1)} aria-label="Next">
                                <span aria-hidden="true" class="flaticon-arrows"></span>
                                  <span class="sr-only">Next</span>
                            </a>
                        </li>
                    </ul>
                </nav> */}
            </div>
        )
    }
}