import React, { Component } from 'react';
import ReactPaginate from 'react-paginate';
import { loadData } from './helpers/loadData';
import { Link } from 'react-router-dom';

export class ProfileModal extends Component {
    constructor(props) {
        super(props);

        const user = JSON.parse(localStorage.getItem('userProfile_' + this.props.steamid64));
        this.state = {
            page: 1,
            user: user === null ? {
                games: {
                    last_page: 1,
                    data: []
                }
            } : user
        };
    }
    
    loadUser(steamid64, page) {
        if (typeof steamid64 === "undefined") steamid64 = this.props.steamid64;
        loadData(`/getProfile?page=${typeof page !== "undefined" ? page : this.state.page}`, {
            steamid64
        })
        .then(data => {
            const { user } = data;
            this.setState({ user });
            localStorage.setItem('userProfile_' + this.props.steamid64, JSON.stringify(user));
        })
        .catch(err => {
            console.error(err);
        });
    }

    componentWillReceiveProps(newProps) {
        if (newProps.steamid64 != this.props.steamid64) {
            this.loadUser(newProps.steamid64);
            const user = JSON.parse(localStorage.getItem('userProfile_' + this.props.steamid64));
            this.setState({
                page: 1,
                user: user === null ? {
                    games: {
                        last_page: 1,
                        data: []
                    }
                } : user
            });
        }
    }

    changePage(page) {
        page = page.selected + 1;
        this.setState({ page });
        this.loadUser(undefined, page);
    }

    render() {
        const { games, page, user } = this.state;

        return (
            <div id="exampleModalCenter" className="modal fade show" tabIndex={-1} role="dialog" aria-labelledby="exampleModalCenterTitle" style={{display: this.props.profileOpened ? 'block' : 'none', paddingRight: 17}}>
            <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                <button type="button" className="close" onClick={this.props.closeProfile} data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <div className="modal-body">
                    {/* <div className="row no-gutters">
                    <div className="col-sm-4  ">
                        <a href="#" className="btn btn-default btn-lg  btn-block">Standard</a>
                    </div>
                    <div className="col-sm-4  ">
                        <a href="#" className="btn btn-default btn-lg    btn-block ">Игры Low</a>
                    </div>
                    <div className="col-sm-4  ">
                        <a href="#" className="btn btn-default btn-lg    btn-block ">Игры Fast</a>
                    </div>
                    </div> */}
                    <div className="d-flex w-100 align-content-center justify-content-xl-between justify-content-center  info-panel flex-wrap flex-sm-nowrap  mb-3">
                    <div className=" mr-2 mb-2 mb-sm-0">
                        <div className="border mb-2"><img src={user.avatar} className="img-fluid" alt width={130} height={130} /></div>
                        {user.trade_link !== null ? <a href={user.trade_link} target="_blank" rel="nofollow" className="btn btn-default   btn-block ">Trade link</a> : ''}
                    </div>
                    <div className="panel">
                        <table className="table-modal table">
                        <tbody><tr>
                            <td>
                                <span className="icon" /> Username:
                            </td>
                            <td>{user.username}</td>
                            </tr>
                            <tr>
                            <td>
                                <span className="icon calend" /> With us from:
                            </td>
                            <td>{user.created_at}</td>
                            </tr>
                            <tr>
                            <td>
                                <span className="icon win" /> Wins (current month):
                            </td>
                            <td>{user.wins}</td>
                            </tr>
                            <tr>
                            <td>
                                <span className="icon persent" /> Winrate (current month):
                            </td>
                            <td>{user.winRate}%</td>
                            </tr>
                            <tr>
                            <td>
                                <span className="icon loos" /> Defeats (current month):
                            </td>
                            <td>{user.loses}%</td>
                            </tr>
                            <tr>
                            <td>
                                <span className="icon money" /> Total win (current month):
                            </td>
                            <td>{user.totalPrice}</td>
                            </tr>
                            <tr>
                                <td colSpan={2}>
                                    <span className="icon burger" />
                                    <a target="_blank" rel="nofollow" href={`https://steamcommunity.com/profiles/${user.steamid64}`} className="hash">https://steamcommunity.com/profiles/{user.steamid64}</a>
                                </td>
                            </tr>
                            <tr>
                                <td colSpan={2} style={{textAlign: 'center'}}>
                                    <a href="#" className="hash" onClick={event => {
                                        event.preventDefault();
                                        this.props.closeProfile();
                                    }}>Close</a>
                                </td>
                            </tr>
                        </tbody></table>
                    </div>
                    </div>
                    <div className="card">
                    <div className="title-row d-flex justify-content-center border-bottom-0 mb-2">
                        History
                    </div>
                    <div className="table-responsive">
                        <table className="table   table-modal-history   table-support">
                        <tbody>
                            {user.games.data.map(game => (
                                <tr>
                                <td><a target="_blank" href={`/game/${game.id}`}>#{game.id}</a></td>
                                <td><span className="table-label">Банк:</span> {game.bank} <i className="flaticon-coins" /></td>
                                <td><span className="table-label">Шанс:</span> <span className="title-color ">{game.winnerChance}%</span></td>
                                <td>
                                    {game.winner_id === null ? <span>In progress...</span> :
                                        (game.winner_id != user.id ? <span class="looz">Defeat</span> : <span class="win">Win</span>)
                                    }
                                </td>
                                </tr>
                            ))}
                        </tbody>
                        </table>
                    </div>
                    <ReactPaginate pageCount={user.games.last_page} activeClassName="active" onPageChange={this.changePage.bind(this)} pageRangeDisplayed={5} pageLinkClassName="page-link" pageClassName="page-item" containerClassName="pagination justify-content-center" marginPagesDisplayed={2} />
                    </div>
                </div>
                </div>
            </div>
            </div>
        )
    }
}