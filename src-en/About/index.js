import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class About extends Component {
    render() {
        return (
            <div className="card-block">
                <div class="title-row d-flex justify-content-center mb-3">About us</div>
                <div className="card">
                    <h5 className="card-title">About us</h5>
                    <div className="card-body">
                        <p className="card-text">Welcome to the PUBGWTF.COM, PlayerUnknown's Battlegrounds jackpot site</p>
                    </div>
                </div>
                <div className="card">
                    <h5 className="card-title">HOW DOES JACKPOT WORKS?</h5>
                    <div className="card-body">
                        <p className="card-text">All ruffle participants deposit their PUBG items. When items count reaches game maximum or timer ends, website choose winner, who is getting all items in game.</p>
                        <p className="card-text">How it works?</p>
                    </div>
                </div>
                <div className="card">
                    <h5 className="card-title">ALL IS PRETTY SIMPLE</h5>
                    <div className="card-body">
                        <ol className="decimal-list">
                            <li>You deposit your PUBG items.</li>
                            <li>Your items joins the game and you receive 100 tickets for every coin of items price</li>
                        </ol>
                    </div>
                </div>
                <div className="card">
                    <h5 className="card-title">MAIN PRINCIPLE</h5>
                    <div className="card-body">
                        <p className="card-text">Higher bet — higher chance! Bet even if you deposit 1 coin you have a chance to win and get all items in game!</p>
                    </div>
                </div>
                <div className="card">
                    <h5 className="card-title">Rules</h5>
                    <div className="card-body">
                        <ol className="decimal-list">
                            <li>Max bet is presented at index page.</li>
                            <li>Website commission — 10%. (If game winner chance greater or equel to 90% commission = 0%)</li>
                            <li>You receive your winning items immediately but sometimes items might be sent with delay</li>
                            <li>If you don't accept trade offer for 1 hour, it cancels. It's required to keep our bots working.</li>
                            <li>Every time you deposit items, you express your agreement with our terms of use</li>
                            <li>If your inventory is closed or you input wrong trade link just contact our support. We resend trade offer for you.</li>
                            <li>Game timer is presented at index page.</li>
                            <li>We accept only PUBG items. Offer contains items from another games will be declined.</li>
                            <li>We can guarantee a correct items prices.</li>
                        </ol>
                    </div>
                </div>
                <div className="card">
                    <h5 className="card-title">Privacy</h5>
                    <div className="card-body">
                        <ol className="decimal-list">
                            <li>We aren't store any info except your steam username, avatar and your trade link</li>
                            <li>Your avatar and username available for everyone in realtime.</li>
                        </ol>
                    </div>
                </div>
            </div>
        )
    }
}