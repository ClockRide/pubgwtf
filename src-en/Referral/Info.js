import React, { Component } from 'react';

export class Info extends Component {
    render() {
        return (
            <div className="card">
                <div className="card-body">
                    {/* <p className="mb-2 text-center">
                    Invite users to PUBGWTF.COM and get coins for their bets
                    </p> */}
                    <div className="text-left text-small">
                    <div className=" mb-2">
                        <a href="#" className="link-bordered">Details about refferal system</a>
                    </div>
                    <ul className="no-style">
                        <li>Player invited by someone (Referral) receives 1% cashback from all his bets</li>
                        <li>Referral can be a player who isn't alredy someone's referral</li>
                        <li>Referral owner receives from 0.5% to 2% from all his bets.</li>
                        <li>Coins received as cashback or reward from referral bets can be exchanged to items in our shop</li>
                        <li>If referral doesn't make bets for 1 month his owner don't receives rewards from his bets anymore</li>
                    </ul>
                    </div>
                </div>
            </div>
        )
    }
}