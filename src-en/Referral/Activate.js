import React, { Component } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import { activateCode } from './activateCode';
import { loadData } from '../helpers/loadData';

export class Activate extends Component {
    constructor(props) {
        super(props);
        const refCode = localStorage.getItem('refCode');

        this.state = {
            refCode: refCode === null ? '' : refCode,
            inputRefCode: ''
        };
        this.loadInfo();
    }

    loadInfo() {
        loadData('/getReferralInfo')
        .then(data => {
            if (!data.success) return toast(data.text, {
                type: toast.TYPE.ERROR
            });
            const { ref_code } = data;
            localStorage.setItem('refCode', ref_code);
            this.setState({ refCode: ref_code });
        });
    }

    render() {
        return (
            <div className="card">
                <ToastContainer />
                <div className="card-body">
                    <form action className="form-link form-referal text-left">
                    <div className="row">
                        <div className="col-12 col-lg-8 offset-lg-2">
                        <label className="form-control-label">Activate referral code</label>
                        <div className="row  no-gutters">
                            <div className="col-sm-8  col-md-9 col-lg-8 ">
                            <div className="form-group">
                                <input type="text" value={this.state.inputRefCode} onChange={event => this.setState({inputRefCode: event.target.value})} className="form-control test-result" placeholder="Input referral code" />
                            </div>
                            </div>
                            <div className="col-sm-4 col-md-3 col-lg-4">
                            <div className="form-group">
                                <button onClick={activateCode.bind(this, this.state.inputRefCode, toast)} type="submit" className="btn btn-block btn-sm btn-default btn-left">Activate</button>
                            </div>
                            </div>
                        </div>
                        <label className="form-control-label">Your referral code</label>
                        <div className="row  no-gutters">
                            <div className="col-sm-8  col-md-9 col-lg-8 ">
                            <div className="form-group">
                                <input type="text" readOnly value={this.state.refCode} className="form-control test-result" placeholder={this.state.refCode} />
                            </div>
                            </div>
                            <div className="col-sm-4 col-md-3 col-lg-4">
                            <div className="form-group">
                                <button type="submit" className="btn btn-block btn-sm btn-default btn-left">Copy</button>
                            </div>
                            </div>
                        </div>
                        <p>Every time someone input your referral code he becomes your referral.</p>
                        <label className="form-control-label">Your referral link</label>
                        <div className="row  no-gutters">
                            <div className="col-sm-8  col-md-9 col-lg-8 ">
                            <div className="form-group">
                                <input type="text" readOnly className="form-control test-result" placeholder={`https://pubgwtf.com/?r=${this.state.refCode}`} />
                            </div>
                            </div>
                            <div className="col-sm-4 col-md-3 col-lg-4">
                            <div className="form-group">
                                <button type="submit" className="btn btn-block btn-sm btn-default btn-left">Copy</button>
                            </div>
                            </div>
                        </div>
                        <p className="mb-0">Every time someone using your link and sign in he becomes your referral.</p>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        )
    }
}