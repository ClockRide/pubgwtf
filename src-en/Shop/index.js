import React, { Component } from 'react';
import { loadData } from '../helpers/loadData';
import { getSumOfProperty } from '../helpers/sumOfProperty';
import { ToastContainer, toast } from 'react-toastify';

export class Shop extends Component {
    constructor(props) {
        super(props);

        const items = JSON.parse(localStorage.getItem('shopItems'));
        const cartItems = JSON.parse(localStorage.getItem('cartItems'));
        const user = JSON.parse(localStorage.getItem('user'));

        this.state = {
            user: user === null ? {} : user,
            items: items === null ? [] : items,
            cart: cartItems === null ? [] : cartItems
        };
        this.loadItems();
        this.loadUser();
    }

    addToCart(itemId, event) {
        event.preventDefault();
        this.setState(prevState => {
            const itemIndex = prevState.items.findIndex(item => item.id == itemId);
            prevState.cart.push(prevState.items[itemIndex]);
            prevState.items.splice(itemIndex, 1);
            return prevState;
        });
    }

    removeFromCart(itemId, event) {
        event.preventDefault();
        this.setState(prevState => {
            const itemIndex = prevState.cart.findIndex(item => item.id == itemId);
            prevState.items.push(prevState.cart[itemIndex]);
            prevState.cart.splice(itemIndex, 1);
            return prevState;
        });
    }

    flushCart() {
        this.state.cart.forEach(item => this.removeFromCart(item.id));
    }

    refreshShop() {
        this.flushCart();
        this.loadItems();
    }

    loadItems() {
        loadData('/shop/getItems')
        .then(data => {
            const { items } = data;
            localStorage.setItem('shopItems', JSON.stringify(items));
            this.setState({
                items
            });
        })
    }

    loadUser() {
        loadData('/getUser')
        .then(data => {
            const { user } = data;

            this.setState({
                user
            });
        })
    }

    proceedPay(event) {
        event.preventDefault();
        
        loadData('/shop/proceedPay', {cart: this.state.cart.map(item => item.id)})
        .then(data => {
            if (data.success) this.refreshShop();
            toast(data.text, {
                type: data.success ? toast.TYPE.SUCCESS : toast.TYPE.ERROR
            });
        })
    }

    render() {
        const { items, cart, user } = this.state;
        return (
            <div>
                <ToastContainer />
                <div className="title-row d-flex justify-content-center mb-2">
                    SHOP
                </div>
                <div className="text-uppercase">CART</div>
                <div className="d-flex flex-wrap flex-md-nowrap">
                    <div className="slide-line slide-roll mb-3 mb-md-0">
                        <ul className="nav flex-nowrap justify-content-start">
                        {cart.map(item => (
                            <li key={item.id} className="shopItem" style={{marginTop: '0px'}}>
                                <a href="#" style={{borderTop: `${item.color} solid 5px`, paddingTop: '0px'}} onClick={this.removeFromCart.bind(this, item.id)}>
                                    <div className="itemsImages-block">
                                        <img src={item.image} className="itemsImages" />
                                    </div>
                                    <div className="caption-top-block">
                                        <h1 className="caption caption-top">{item.name}</h1>
                                    </div>
                                    <span className="caption caption-bot">{parseFloat(item.shop_price).toFixed(2)}
                                        <i className="flaticon-coins" />
                                    </span>
                                </a>
                            </li>
                        ))}
                        </ul>
                    </div>
                    <div className="card basket mx-auto ml-md-1">
                        <div>Balance: <strong>{user === null ? '' : parseFloat(user.money).toFixed(2)}</strong><i className="flaticon-coins color" /></div>
                        <div>Total price: <strong>{getSumOfProperty(cart, 'shop_price').toFixed(2)}</strong><i className="flaticon-coins color" /></div>
                        <div className="d-flex justify-content-around mt-2 line-btn">
                            <a href="#" className="btn danger" onClick={this.flushCart.bind(this)}>
                                <i className="flaticon-commerce" />
                            </a>
                            <a href="#" className="btn" onClick={this.refreshShop.bind(this)}>
                                <i className="flaticon-exchange" />
                            </a>
                            <a href="#" onClick={this.proceedPay.bind(this)} className="btn success ">
                                <i className="flaticon-add-to-cart" />
                            </a>
                        </div>
                    </div>
                </div>
                <hr />
                <div>
                    <div className="shop">
                        <ul className="itemList nav">
                        {items.map(item => (
                            <li className="shopItem" key={item.id} style={{borderBottomColor: item.color}}>
                                <a href="#" style={{borderTop: `${item.color} solid 5px`, paddingTop: '0px'}} onClick={this.addToCart.bind(this, item.id)}>
                                    <div className="itemsImages-block">
                                        <img className="itemsImages" src={item.image} />    
                                    </div>
                                    <div className="caption-top-block">
                                        <h1 className="caption caption-top">{item.name} <br /> {/*item.rarity.replace('#', '')*/}</h1>
                                    </div>
                                    <span className="caption caption-bot"> {parseFloat(item.shop_price).toFixed(2)} <i className="flaticon-coins" /></span>
                                </a>
                            </li>
                        ))}
                        </ul>
                    </div>
                    {/* <nav aria-label="Page navigation example">
                        <ul className="pagination mt-1  justify-content-center">
                        <li className="page-item">
                            <a className="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true" className="flaticon-left-arrow" />
                            <span className="sr-only">Previous</span>
                            </a>
                        </li>
                        <li className="page-item"><a className="page-link active" href="#">1</a></li>
                        <li className="page-item"><a className="page-link" href="#">2</a></li>
                        <li className="page-item"><a className="page-link" href="#">3</a></li>
                        <li className="page-item"><a className="page-link" href="#">4</a></li>
                        <li className="page-item"><a className="page-link" href="#">5</a></li>
                        <li className="page-item">
                            <a className="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true" className="flaticon-arrows" />
                            <span className="sr-only">Next</span>
                            </a>
                        </li>
                        </ul>
                    </nav> */}
                    {/* <div className="card">
                        <div className="card-body p-3">
                        <div className="title text-center mb-3">Рулетка CS GO для бомжей с минимальной ставкой 1 рубль</div>
                        <p>Рулетки кс го от 1 рубля – это сервисы обмена виртуальными предметами между игроками Steam. Продолжительность новой игры длиться 120 секунд. Игра запускается только после того, как присоединяется второй участник. Честные рулетки кс го для бомжей, как правило, принимают ставки CS GO от 1 рубля. Максимально допустимое количество вещей для раунда 100 предметов. Чтобы сделать свои первые ставки CS GO скинами, пройдите авторизацию на нашем сайте через специальное приложение Steam и мобильный аунтефикатор.</p>
                        </div>
                    </div> */}
                </div>
            </div>
        )
    }
}