rollsound  = new Audio('/assets/sounds/scroll.mp3');

$(document).ready(function () {

    $('.game-tab').click(function(){
        $('.game-tab').removeClass('active');
        $(this).addClass('active');
        $('[data-tab]').hide();
        $('[data-tab='+$(this).data('game')+']').show();
        $('.buy-btn-sm').data('game', $(this).data('game'));
        updateTitle();
    })

    $(".add_balance").click(function(e){
        e.preventDefault();
        $(".modal1").fadeIn();
    });
    $(".close").click(function(e){
        e.preventDefault();
        $(".modal1").fadeOut();
        $(".modal2").fadeOut();
    });
    
    $(".but_comr2").click(function(e){
        e.preventDefault();
        $(".modal2").fadeIn();
    });
    
    $(".whit_cart span").click(function(){
        $(".con_whit_cart").slideToggle();
        
        if ( !$(".whit_cart span").hasClass("active") ){
            $(".whit_cart span").addClass("active");
        }else{
            $(".whit_cart span").removeClass("active");
        }
    });

    $('.shop-list .deposit-item').click(function(){
        $(this).toggleClass('item-selected');
        $('.shop-list .deposit-item .coins').removeClass('white').addClass('orange');
        $('.shop-list .item-selected .coins').removeClass('orange').addClass('white');
        var total = 0;
        $('.item-selected').each(function(i, item){
            total += parseInt($(item).find('.deposit-price').text());
        })
        $('#sum').text(total);
    })
    $('#accept').change(function(){
        $('.submit-button').prop('disabled', function(i, v) { return !v; });
    })
    $('.submit-button').click(function(){
        var items = [];
        $('.item-selected').each(function(i, item){
            items.push($(item).data('original-title'));
        })
        $.ajax({
            url: '/shop/buy',
            type: 'POST',
            dataType: 'json',
            data: {items: items, price: $('#sum').text()},
            success: function (data) {
                if (data.status == 'success') {
                    $('#linkBlock').slideUp();
                    $('.no-link').removeClass('no-link');
                    if (data.msg) return $.notify(data.msg, 'success');
                }
                if (data.msg) $.notify(data.msg, 'error');
            },
            error: function () {
                ajaxError();
            }
        });
    })

    $('a[href="' + document.location.pathname + '"]').parent().addClass('active');

    $('.history-block-item .user .username').each(function () {
        $(this).text(replaceLogin($(this).text()));
    });

    $('.weapon_deposit:not(.card)').tooltip({container: 'body'});

    $('[data-toggle="popover"]').popover({
        "container": "body"
    });

    EZYSKINS.init();

    $('.close-eto-delo').click(function (e) {
        $(this).parent('.msg-wrap').slideUp();
    });

    if ($.cookie('nosound') == 'true') {$('body').addClass('nosound');rollsound.muted=true;}
    $(document).on('click', '.sound-control', function () {
        $('body').toggleClass('nosound');
        rollsound.muted = $('body').hasClass('nosound')
        $.cookie('nosound', rollsound.muted);
        return false;
    });

    $(document).on('click', '.no-link', function (e) {
        $('#linkBlock').slideDown();
        return false;
    });

    $(document).on('click', '#cardDepModal', function () {
        if ($(this).hasClass('no-link')) return;
        $('#cardDepositModal .cards-block-up-btn').show();
        $('#cardDepositModal .box-modal-footer').show();
        $('#cardDepositModal').arcticmodal();
    });

    $(document).on('click', '.add-balance', function () {
        $('#cardDepositModal .cards-block-up-btn').hide();
        $('#cardDepositModal .box-modal-footer').hide();
        $('#cardDepositModal').arcticmodal();
    });

    $(document).on('click', '.deposit-no-link', function () {
        $('#linkBlock').slideDown();
        return false;
    });

    $(document).on('click', '.tooltip-btn.level', function () {
        $('.profile-level').tooltip('hide');
        $('#level-popup').arcticmodal();
    });
    $(document).on('click', '.tooltip-btn.card', function () {
        $('.weapon_deposit.card').tooltip('hide');
        $('#card-popup').arcticmodal();
    });
    $(document).on('click', '.tooltip-btn.ticket', function () {
        $('.ticket-number').tooltip('hide');
        $('#ticket-popup').arcticmodal();
    });
    $(document).on('click', '#user-level-btn', function () {
        $('.level-badge').tooltip('hide');
        $('#level-popup').arcticmodal();
    });

    $('.tooltip').remove();

    $('.ticket-number').tooltip({
        html: true,
        trigger: 'hover',
        delay: {show: 50, hide: 200},
        title: function () {
            var text = '1 билет = 1 коп';
            var btn = 'для чего нужен билет?';

            return '<span class="tooltip-title ticket">' + text + '</span>';
        }
    });

    $('.weapon_deposit:not(.card)').tooltip({
        container: 'body',
        //delay: {show: 50, hide: 200}
    });

    $('.weapon_deposit.card').each(function () {
        var that = $(this);
        that.data('old-title', that.attr('title'));
        that.attr('title', null);
        that.tooltip({
            html: true,
            trigger: 'hover',
            delay: {show: 50, hide: 200},
            title: function () {
                var text = $(this).data('old-title');
                var btn = 'для чего нужны карточки?';

                return '<span class="tooltip-title card">' + text + '</span>';
            }
        });
    });


    $('.save-trade-link-input')
        .keypress(function (e) {
            if (e.which == 13) $(this).next().click()
        })
        .on('paste', function () {
            var that = $(this);
            setTimeout(function () {
                that.next().click();
            }, 0);
        });

    $('.save-trade-link-input-btn').click(function () {
        var that = $(this).prev();
        $.ajax({
            url: '/settings/save',
            type: 'POST',
            dataType: 'json',
            data: {trade_link: $(this).prev().val()},
            success: function (data) {
                if (data.status == 'success') {
                    $('#linkBlock').slideUp();
                    $('.no-link').removeClass('no-link');
                    if (data.msg) return $.notify(data.msg, 'success');
                }
                if (data.msg) $.notify(data.msg, 'error');
            },
            error: function () {
                ajaxError();
            }
        });
        return false;
    });

    $('.tooltip').remove();
    $('.current_user').tooltip({container: 'body'});

});

function updateBackground() {
    var mainHeight = $('.dad-container').height();
    var windowHeight = $(window).height();

    if (mainHeight > windowHeight) {
        $('.main-container').height('auto');
    }
    else {
        $('.main-container').css('min-height', '100%');
    }
}

function replaceLogin(login) {
    function replacer(match, p1, p2, p3, offset, string) {
        var links = ['CSGOBOMJ.RU'];
        return links.indexOf(match.toLowerCase()) == -1 ? '' : match;
    }

    login = login.replace('сom', 'com').replace('cоm', 'com').replace('соm', 'com');
    var res = login.replace(/([а-яa-z0-9-]+) *\. *(ru|com|net|gl|su|red|ws|us)+/i, replacer);
    if (!res.trim()) {

        var check = login.toLowerCase().split('CSGOBOMJ.RU').length > 1;

        if (check) {
            res = login;
        }
        else {
            res = login.replace(/csgo/i, '').replace(/ *\. *ru/i, '').replace(/ *\. *com/i, '');
            if (!res.trim()) {
                res = 'UNKNOWN';
            }
        }
    }

    res = res.split('script').join('srcipt');
    return res;
}

function updateScrollbar() {
    $('.current-chance-block').perfectScrollbar('destroy');
    $('.current-chance-block').perfectScrollbar({suppressScrollY: true, useBothWheelAxes: true});
}

// updateScrollbar();
updateBackground();

function getRarity(type) {
    var rarity = '';
    var arr = type.split(',');
    if (arr.length == 2) type = arr[1].trim();
    if (arr.length == 3) type = arr[2].trim();
    if (arr.length && arr[0] == 'Нож') type = '★';
    switch (type) {
        case 'Армейское качество':
            rarity = 'milspec';
            break;
        case 'Запрещенное':
            rarity = 'restricted';
            break;
        case 'Засекреченное':
            rarity = 'classified';
            break;
        case 'Тайное':
            rarity = 'covert';
            break;
        case 'Ширпотреб':
            rarity = 'common';
            break;
        case 'Промышленное качество':
            rarity = 'common';
            break;
        case '★':
            rarity = 'rare';
            break;
        case 'card':
            rarity = 'card';
            break;
    }
    return rarity;
}

function n2w(n, w) {
    n %= 100;
    if (n > 19) n %= 10;

    switch (n) {
        case 1:
            return w[0];
        case 2:
        case 3:
        case 4:
            return w[1];
        default:
            return w[2];
    }
}

function lpad(str, length) {
    while (str.toString().length < length)
        str = '0' + str;
    return str;
}

$(document).on('click', '#showUsers, #showItems', function () {
    if ($(this).is('.active')) return;

    $('#showUsers, #showItems').removeClass('active');
    $(this).addClass('active');

    $('#usersChances .users').toggle();
    $('#usersChances .items').toggle();
    updateScrollbar();
});

$('#usersChances').hover(function () {
    var block = $('#usersChances .current-chance-block.users');
    var min = 10;

    if (block.find('.current-chance-wrap').children().length > min) $('.arrowscroll').show();
}, function () {
    $('.arrowscroll').hide();
});
$('.arrowscroll').click(function () {
    var block = $('#showUsers').is('.active') ? $('.current-chance-block.users') : $('.current-chance-block.items');
    var direction = $(this).is('.left') ? '-' : '+';

    block
        .stop(true, false)
        .animate({scrollLeft: direction + "=250"});
});

if (START) {
    updateBackground();
     var socket = io.connect('csgobomj.ru:2082');

    if (checkUrl()) {
        socket
            .on('connect', function () {
                $('#loader').hide();
            })
            .on('disconnect', function () {
                $('#loader').show();
            })
            .on('online', function (data) {
                $('.stats-onlineNow').text(Math.abs(data));
            })
            .on('newDeposit', function (data) {
                var audio = new Audio('/assets/sounds/bet.mp3');
                if ($('.game-tab[data-game=0]').hasClass('active') && !$('body').hasClass('nosound')) audio.play();
                updateBackground();
                data = JSON.parse(data);
                var pblock = $('.game-tab[data-game=0] .plus');
                $(pblock).find('span').text(Math.round(data.gamePrice) - $('.roundBank:eq(0)').text());
                $(pblock).show();
                setTimeout(function(){$(pblock).hide()}, 1000);
                $('#deposits').prepend(data.html);
                var username = $('#bet_' + data.id + ' .history-block-item .user .username').text();
                $('#bet_' + data.id + ' .history-block-item .user .username').text(replaceLogin(username));
                $('.roundBank').html(Math.round(data.gamePrice));
                updateTitle(Math.round(data.gamePrice));
                // $('title').text(Math.round(data.gamePrice) + ' руб - CSGOBOMJ.RU');
                $('#barContainer .item-bar-text').html('<span>' + data.itemsCount + ' / 100</span>' + n2w(data.itemsCount, [' предмет', ' предмета', ' предметов']));
                $('#barContainer .item-bar').css('width', data.itemsCount + '%');
                $('.weapon_deposit').tooltip({container: 'body', placement: 'top'});

                html_chances = '';

                data.chances = sortByChance(data.chances);
                data.chances.forEach(function (info) {
                    if (USER_INNER == info.innerid) {
                        $('#myItemsCount').html(info.items + '<span style="font-size: 12px;">' + n2w(info.items, [' предмет', ' предмета', ' предметов']) + '</span>');
                        $('#myChance').text(info.chance + '%');
                    }
                    $('.id-' + info.innerid).text(info.chance + '%');
                    html_chances += '<div class="current_user" title="' + replaceLogin(info.username) + '"><a href="/user/' + info.innerid + '" target="_blank" class="user_ph_curr"><img src="' + info.avatar + '"></a><div class="shance">' + info.chance + '%</div></div>';
                });

                $('#usersChances').html(html_chances);
                $('#usersChances').slideDown();

                $('.tooltip').remove();
                $('.current_user').tooltip({container: 'body'});

                EZYSKINS.initTheme();
            })
            .on('forceClose', function () {
                $('.forceClose').removeClass('msgs-not-visible');
            })
            .on('timer', function (time) {
                var audio = new Audio('/assets/sounds/timer-tick-quiet.mp3');
                if ($('.game-tab[data-game=0]').hasClass('active') && !$('body').hasClass('nosound') && time <= 25) audio.play();
                $('.gameTimer .countMinutes').text(lpad(Math.floor(time / 60), 2));
                $('.gameTimer .countSeconds').text(lpad(time - Math.floor(time / 60) * 60, 2));
            })
            .on('slider', function (data) {

                $('.gameTimer .countMinutes').text('--');
                $('.gameTimer .countSeconds').text('--');

                // Таймер
                $('#newGameTimer .countSeconds').text(lpad(data.time - Math.floor(data.time / 60) * 60, 2));

                if (ngtimerStatus) {
                    ngtimerStatus = false;
                    var users = data.users;

                    users = mulAndShuffle(users, 130, data.winner);
                    users[112] = data.winner;
                    html = '';
                    users.forEach(function (i) {
                        html += '<div class="ruser_b"><img src="'+i.avatar+'"></div>';
                    });
                    $('#usersCarousel').html(html);

                    $('#barContainer').slideUp();
                    $('#usersCarouselConatiner').slideDown();

                    if (data.showCarousel) {
                        $('#depositButtonsBlock').slideUp();
                    }
                    else {
                        $('#depositButtonsBlock').slideUp();
                    }

                    $('#winnerInfo').slideDown();

                    fillWinnerInfo(data);

                    $('#roundFinishBlock .number').text(data.round_number);
                    $('#roundFinishBlock .secret').text(data.round_secret);
                    // rollsound.play();
                    $('#roundFinishBlock .date').html(data.date + '<span>' + data.date_hours + '</span>');


                    $('#usersCarousel').css('margin-left', -41);
                    if (data.showSlider) {
                        $('#usersCarousel').animate(
                            {marginLeft: -7917}, 1000 * 10,
                            function () {
                                $('#winnerInfo .winner-info-holder').slideDown();
                                $('#roundFinishBlock').slideDown();
                            });
                    }

                    function fillWinnerInfo(data) {
                        data = data || {winner: {}};

                        var obj = {
                            totalPrice: data.game.price || 0,
                            number: data.game.price ? ('#' + Math.floor(data.round_number * data.game.price)) : '???',
                            tickets: data.tickets || 0,
                            winner: {
                                image: data.winner.avatar || '???',
                                login: data.winner.username || '???',
                                id: data.winner.innerid || '#',
                                chance: data.chance || 0,
                                winTicket: data.ticket || '???'
                            }
                        };
                        $('#winnerInfo #winTicket').text('#' + obj.winner.winTicket);
                        $('#winnerInfo #totalTickets').text(obj.tickets);
                        $('#winnerInfo img').attr('src', obj.winner.image);
                        $('#winnerInfo #winnerLink').text(replaceLogin(obj.winner.login));
                        $('#winnerInfo #winnerLink').attr('href', '/user/' + obj.winner.id);
                        $('#winnerInfo #winnerChance').text(obj.winner.chance.toFixed(2));
                        $('#winnerInfo #winnerSum').text(obj.totalPrice);
                    }
                }
            })
            .on('newGame', function (data) {
                updateBackground();
                $('.check-hash').attr('href', '/fairplay/' + data.id);
                $('#usersChances').html('');
                $('#usersChances').slideUp();
                $('#deposits').html('');
                $('#myItemsCount').html('0 <span style="font-size: 12px;"> предметов</span>');
                $('#myChance').text('0%');
                $('#roundId').text(data.id);
                $('.roundBank').html('0');
                $('#hash').text(data.hash);
                $('#barContainer .item-bar-text').html('<span>0</span> предметов');
                $('#barContainer .item-bar').css('width', '0%');
                $('#roundFinishBlock').slideUp();
                $('#barContainer').slideDown();
                $('#usersCarouselConatiner').slideUp();
                $('#depositButtonsBlock').slideDown();
                $('#winnerInfo').slideUp();
                $('#winnerInfo .winner-info-holder').slideUp();
                $('#roundFinishBlock').slideUp();
                $('.gameTimer .countMinutes').text('02');
                $('.gameTimer .countSeconds').text('00');
                updateTitle(0);
                $('#roundStartBlock .date').html(formatDate(data.created_at));
                setTimeout(updateBackground, 1000);
                ngtimerStatus = true;
            })
            .on('lastwinner', function (data) {
                $('#winidrest').text(data.username);
                $('#winavatar img').attr('src', data.avatar);
                $('#winchancet span').text(data.percent + '%');
                $('#winmoner span').text(data.price + ' Р');
            })
            .on('all_lucky', function (data) {
                $('#vsegda-1').text(data.username);
                $('#vsegda-4 img').attr('src', data.avatar);
                $('#winchancet-2 span').text(data.percent + '%');
                $('#winchancet-2 span').text(data.price + ' Р');
            })
            .on('all_lucky_today', function (data) {
                $('#denb-1').text(data.username);
                $('#denb-4 img').attr('src', data.avatar);
                $('#denb-3 span').text(data.percent + '%');
                $('#denb-2 span').text(data.price + ' Р');
            })
            .on('queue', function (data) {
                if (data) {
                    var n = data.indexOf(USER_ID);
                    if (n !== -1) {
                        $.notify('Ваш депозит обрабатывается', {
                            clickToHide: 'false',
                            autoHide: "false",
                            className: "success"
                        });
                    }
                }
            })
            .on('chat_messages', function (data) {
                update_chat(); return;
                message = data;
                if (message && message.length > 0) {
                    $('#messages').html('');
                    for (var i in message) {
                        var a = $("#chatScroll")[0];
                        // var isScrollDown = (a.offsetHeight + a.scrollTop) == a.scrollHeight;
                        var isScrollDown = Math.abs((a.offsetHeight + a.scrollTop) - a.scrollHeight) < 5;

                        if (message[i].admin != 1) {
                            var html = '<div class="chatMessage clearfix" data-uuid="' + message[i].id + '" data-user="' + message[i].userid + '">';
                            html += '<a href="/user/' + message[i].userid + '" target="_blank"><img src="https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/' + _.escape(message[i].avatar).replace('_full', '') + '"></a>';
                            html += '<div class="login" href="/user/' + message[i].userid + '" target="_blank">' + message[i].username + '</div>';
                            html += '<div class="body">' + message[i].messages + '</div>';
                            html += '</div>';
                        }
                        else {
                            var html = '<div class="chatMessage clearfix" data-uuid="' + message[i].id + '">';
                             html += '<a href="/user/' + message[i].userid + '" target="_blank"><img src="https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/' + _.escape(message[i].avatar).replace('_full', '') + '"></a>';
                            html += '<div class="login" href="/user/' + message[i].userid + '" target="_blank">' + message[i].username + ' <span class="color-orange">[Админ]</span></div>';
                            html += '<div class="body">' + message[i].messages + '</div>';
                            html += '</div>';
                        }
                        $('#messages').append(html);
                        if ($('.chatMessage').length > 100) {
                            // $('.chatMessage').eq(0).remove();
                        }
                    }

                    if (isScrollDown) a.scrollTop = a.scrollHeight;
                    $("#chatScroll").perfectScrollbar('update');
                }
            })
            .on('depositDecline', function (data) {
                data = JSON.parse(data);
                if (data.user == USER_ID) {
                    clearTimeout(declineTimeout);
                    declineTimeout = setTimeout(function () {
                        $('#errorBlock').slideUp();
                    }, 1000 * 10)
                    $('#errorBlock p').text(data.msg);
                    $('#errorBlock').slideDown();
                }
            })
        
    }
    else {
        socket
            .on('online', function (data) {
                $('.stats-onlineNow').text(Math.abs(data));
            })
            .on('queue', function (data) {
                if (data) {
                    var n = data.indexOf(USER_ID);
                    if (n !== -1) {
                        $.notify('Ваш депозит обрабатывается', {
                            clickToHide: 'false',
                            autoHide: "false",
                            className: "success"
                        });
                    }
                }
            })
    }
    var declineTimeout,
        timerStatus = true,
        ngtimerStatus = true;
}

function loadMyInventory() {
    $('thead').hide();
    $.ajax({
        url: '/myinventory',
        type: 'POST',
        dataType: 'json',
        success: function (data) {
            var text = '<tr><td colspan="4" style="text-align: center">РџСЂРѕРёР·РѕС€Р»Р° РѕС€РёР±РєР°. РџРѕРїСЂРѕР±СѓР№С‚Рµ РµС‰Рµ СЂР°Р·</td></tr>';
            var totalPrice = 0;

            if (!data.success && data.Error) text = '<tr><td colspan="4" style="text-align: center">' + data.Error + '</td></tr>';

            if (data.success && data.rgInventory && data.rgDescriptions) {
                text = '';
                var items = mergeWithDescriptions(data.rgInventory, data.rgDescriptions);
                console.table(items);
                items.sort(function (a, b) {
                    return parseFloat(b.price) - parseFloat(a.price)
                });
                _.each(items, function (item) {
                    item.price = item.price || 0;
                    totalPrice += parseFloat(item.price);
                    item.price = item.price;
                    item.image = 'https://steamcommunity-a.akamaihd.net/economy/image/class/730/' + item.classid + '/200fx200f';
                    item.market_name = item.market_name || '';
                    text += ''
                        + '<tr>'
                        + '<td class="item-image"><div class="item-image-wrap">' + '<img src="' + item.image + '">' + '</div></td>'
                        + '<td class="item-name">' + item.name + '</td>'
                        + '<td class="item-type">' + item.market_name.replace(item.name, '').replace('(', '').replace(')', '') + '</td>'
                        + '<td class="item-cost">' + (item.price || '---') + '</td>'
                        + '</tr>'
                });
                $('#totalPrice').text(totalPrice.toFixed(2));
                $('thead').show();
            }

            $('tbody').html(text);
            updateBackground();
        },
        error: function () {
            var text = isEn() ? 'An error has occurred. Try again' : 'РџСЂРѕРёР·РѕС€Р»Р° РѕС€РёР±РєР°. РџРѕРїСЂРѕР±СѓР№С‚Рµ РµС‰Рµ СЂР°Р·';
            $('tbody').html('<tr><td colspan="4" style="text-align: center">' + text + '<td></tr>');
        }
    });
}

function mergeWithDescriptions(items, descriptions) {
    return Object.keys(items).map(function (id) {
        var item = items[id];
        var description = descriptions[item.classid + '_' + (item.instanceid || '0')];
        for (var key in description) {
            item[key] = description[key];

            delete item['icon_url'];
            delete item['icon_drag_url'];
            delete item['icon_url_large'];
        }
        return item;
    })
}

function shuffleArr(a) {
    for (let i = a.length; i; i--) {
        let j = Math.floor(Math.random() * i);
        [a[i - 1], a[j]] = [a[j], a[i - 1]];
    }
}


function mulAndShuffle(users, total, winner) {
    Array.prototype.move = function (old_index, new_index) {
        if (new_index >= this.length) {
            var k = new_index - this.length;
            while ((k--) + 1) {
                this.push(undefined);
            }
        }
        this.splice(new_index, 0, this.splice(old_index, 1)[0]);
        return this; // for testing purposes
    };
    var res = [];
    for (u in users) {
        for (var i = 0; i < Math.round(total * (users[u].chance / 100)); i++) {
            res.push(users[u])
        }
    }
    shuffleArr(res);
    for (u in res) {
        if (res[u].innerid == winner.innerid) {
            res.move(u, 112);
            break;
        }
    }
    return res;
}

$(document).on('click', '.vote', function () {
    var that = $(this);
    $.ajax({
        url: '/ajax',
        type: 'POST',
        dataType: 'json',
        data: {action: 'voteUser', id: $(this).data('profile')},
        success: function (data) {
            if (data.status == 'success') {
                $('#myProfile').find('.votes').text(data.votes || 0);
            }
            else {
                if (data.msg) that.notify(data.msg, {position: 'bottom middle', className: "error"});
            }
        },
        error: function () {
            that.notify("Произошла ошибка. Попробуйте еще раз", {position: 'bottom middle', className: "error"});
        }
    });
});

function sortByChance(arrayPtr) {
    var temp = [],
        item = 0;
    for (var counter = 0; counter < arrayPtr.length; counter++) {
        temp = arrayPtr[counter];
        item = counter - 1;
        while (item >= 0 && arrayPtr[item].chance < temp.chance) {
            arrayPtr[item + 1] = arrayPtr[item];
            arrayPtr[item] = temp;
            item--;
        }
    }
    return arrayPtr;
}

function checkUrl() {
    var pathname = window.location.pathname;

    if (pathname.indexOf('game') + 1) {
        return false;
    }
    else {
        return true;
    }

}

function formatDate(date) {
    return moment(date).format('DD/MM/YYYY') + ' <span>' + moment(date).format('hh:mm') + '</span>';
}

function updateTitle(sum){
    var main = $('.game-tab.active .bank_game>span').text();
    if (sum == undefined && ! main) return;
    // $('title').text((main ? main : sum) + ' руб - CSGOBOMJ.RU');
}

$.notify.addStyle('custom', {html: "<div>\n<span data-notify-text></span>\n</div>"});
$.notify.defaults({style: "custom"});

$(document).on('mouseenter', '.iusers, .iskins', function () {
    $(this).tooltip('show');
});

$('#ref-setup-frm').submit(function(e){
    $.ajax({
        url: '/ref',
        type: 'POST',
        dataType: 'json',
        data: {
            title: $('.ref-setup input[name=title]').val(),
            _token: $('.ref-setup input[name=_token]').val(),
            submit: $('.ref-setup input[name=submit]').val()
        },
        success: function (data) {
            if (data.success) {
                $('.title-block h2').text('Ваш реферальный код: ' + data.refcode);
                $('.ref-info').text(data.success).slideDown(300);
                $('.ref-setup').slideUp(300);
            } else if (data.errors) {
                $('.ref-info').text(data.errors).slideDown(300);
            }
        },
        error: function () {
            // 
        }
    });
    e.originalEvent.preventDefault();
})
$('#ref-connect-frm').submit(function(e){
    $.ajax({
        url: '/ref',
        type: 'POST',
        dataType: 'json',
        data: {
            title: $('.ref-connect input[name=title]').val(),
            _token: $('.ref-connect input[name=_token]').val(),
            refer: $('.ref-connect input[name=refer]').val()
        },
        success: function (data) {
            if (data.success) {
                $('.ref-info').text(data.success).slideDown(300);
                $('.ref-balance').text(data.balance);
                $('.ref-connect').slideUp(300);
            } else if (data.errors) {
                $('.ref-info').text(data.errors).slideDown(300);
            }
        },
        error: function () {
            // 
        }
    });
    e.originalEvent.preventDefault();
})


$('#promo-form').submit(function(e){
    $.ajax({
        url: '/promo/enter',
        type: 'POST',
        dataType: 'json',
        data: {
            promo: $('input[name=promo]').val(),
            _token: $('input[name=_token]').val()
        },
        success: function (data) {
            if (data.success) {
                $.notify(data.success, 'success');
            } else if (data.error) {
                $.notify(data.error, 'error');
            }
        },
        error: function (d) {
            console.log(d)
        }
    });
    e.originalEvent.preventDefault();
})

