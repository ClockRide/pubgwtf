import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {loadData} from './helpers/loadData';

export class User extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: JSON.parse(localStorage.getItem('user'))
        };
        this.getUser();
    }

    getUser() {
        loadData('/getUser')
        .then(data => {
            const { user } = data;
            this.setState({
                user
            });
            localStorage.setItem('user', JSON.stringify(user));
        })
    }



    render() {
        const { user } = this.state;
        
        return user === null ? (
            <a href="/login" className="btn btn-lg btn-sign btn-block"><span>Войти</span></a>
        ) : (
            <div className=" d-flex align-items-center  authorisation">
                <a href={`http://steamcommunity.com/profiles/${user.steamid64}`} rel="nofollow" target="_blank" className="avatar"><img src={user.avatar} className="img-fluid" alt /></a>
                <div className="mr-3">
                    <a href="#" className="nik">{user.username}</a>
                    <div className="d-flex"><Link to="/settings" className="mr-3 nowrap"><i className="flaticon-settings-work-tool" /> <span>Настройки</span></Link>
                    <a><span>Баланс: </span><span className="nowrap">{parseFloat(user.money).toFixed(2)}<i className="flaticon-coins" /></span></a>
                    </div>
                </div>
                <a href={`/logout?a=${Math.random()}`} className="btn btn-exit ml-auto "><i className="flaticon-arrow" /></a>
            </div>
        )
    }
}