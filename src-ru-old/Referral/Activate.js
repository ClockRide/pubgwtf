import React, { Component } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import { activateCode } from './activateCode';
import { loadData } from '../helpers/loadData';
import copy from 'copy-to-clipboard';

export class Activate extends Component {
    constructor(props) {
        super(props);
        const refCode = localStorage.getItem('refCode');

        this.state = {
            refCode: refCode === null ? '' : refCode,
            inputRefCode: ''
        };
        this.loadInfo();
    }

    loadInfo() {
        loadData('/getReferralInfo')
        .then(data => {
            if (!data.success) return toast(data.text, {
                type: toast.TYPE.ERROR
            });
            const { ref_code } = data;
            localStorage.setItem('refCode', ref_code);
            this.setState({ refCode: ref_code });
        });
    }

    render() {
        return (
            <div className="card">
                <ToastContainer />
                <div className="card-body">
                    <form action className="form-link form-referal text-left">
                    <div className="row">
                        <div className="col-12 col-lg-8 offset-lg-2">
                        <label className="form-control-label">Активировать реферальный код</label>
                        <div className="row  no-gutters">
                            <div className="col-sm-8  col-md-9 col-lg-8 ">
                            <div className="form-group">
                                <input type="text" value={this.state.inputRefCode} onChange={event => this.setState({inputRefCode: event.target.value})} className="form-control test-result" placeholder="Введите реферальный код" />
                            </div>
                            </div>
                            <div className="col-sm-4 col-md-3 col-lg-4">
                            <div className="form-group">
                                <button onClick={activateCode.bind(this, this.state.inputRefCode, toast)} type="submit" className="btn btn-block btn-sm btn-default btn-left">Активировать</button>
                            </div>
                            </div>
                        </div>
                        <label className="form-control-label">Ваш реферальный код</label>
                        <div className="row  no-gutters">
                            <div className="col-sm-8  col-md-9 col-lg-8 ">
                            <div className="form-group">
                                <input type="text" readOnly value={this.state.refCode} className="form-control test-result" placeholder={this.state.refCode} />
                            </div>
                            </div>
                            <div className="col-sm-4 col-md-3 col-lg-4">
                            <div className="form-group">
                                <button type="submit" onClick={copy.bind(this, this.state.refCode)} className="btn btn-block btn-sm btn-default btn-left">Копировать</button>
                            </div>
                            </div>
                        </div>
                        <p>Каждый раз когда пользователь вводит ваш реферальный код он становится вашим рефералом.</p>
                        <label className="form-control-label">Ваша реферальная ссылка</label>
                        <div className="row  no-gutters">
                            <div className="col-sm-8  col-md-9 col-lg-8 ">
                            <div className="form-group">
                                <input type="text" readOnly className="form-control test-result" placeholder={`https://betsdota22.ru/?r=${this.state.refCode}`} />
                            </div>
                            </div>
                            <div className="col-sm-4 col-md-3 col-lg-4">
                            <div className="form-group">
                                <button type="submit" onClick={copy.bind(this, `https://betsdota22.ru/?r=${this.state.refCode}`)} className="btn btn-block btn-sm btn-default btn-left">Копировать</button>
                            </div>
                            </div>
                        </div>
                        <p className="mb-0">Каждый раз вы вводите чей-то реферальный код, вы ставновитесь его рефералом.</p>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        )
    }
}