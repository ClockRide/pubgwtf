import React, { Component } from 'react';
import { Info } from './Info';
import { Activate } from './Activate';
import { Level } from './Level';

export class Referral extends Component {
    render() {
        return (
            <div>
                <div class="title-row d-flex justify-content-center mb-2">
                    Реферальная система
                </div>
                <Info />
                <Activate />
                <Level />
            </div>
        )
    }
}