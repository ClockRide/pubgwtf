import React, { Component } from 'react';

export class Info extends Component {
    render() {
        return (
            <div className="card">
                <div className="card-body">
                    {/* <p className="mb-2 text-center">
                    Invite users to PUBGWTF.COM and get coins for their bets
                    </p> */}
                    <div className="text-left text-small">
                    <div className=" mb-2">
                        <a href="#" className="link-bordered">Детали о реферальной система</a>
                    </div>
                    <ul className="no-style">
                        <li>Игрок приглашённый кем-то (реферал) получает 1% от всех его ставок</li>
                        <li>Каждый игрок может стать рефералом только одного человека</li>
                        <li>Пригласивший реферела получает от 0.5% до 2% от всех его ставок.</li>
                        <li>Коины полученные из реферальной системы могут быть использованы в нашем магазине</li>
                        <li>Если реферал не ставит ставок в течении 1 месяца пригласивший его перестаёт получать процент от его ставок</li>
                    </ul>
                    </div>
                </div>
            </div>
        )
    }
}