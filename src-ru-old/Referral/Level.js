import React, { Component } from 'react';
import { loadData } from '../helpers/loadData';
import { ToastContainer, toast } from 'react-toastify';

export class Level extends Component {
    constructor(props) {
        super(props);
        const referrals = JSON.parse(localStorage.getItem('referrals'));

        this.state = {
            myReferralsShown: false,
            percent: 0.005,
            totalProfit: 0,
            referrals: referrals === null ? [] : referrals
        };
        this.loadInfo();
    }

    loadInfo() {
        loadData('/getReferralInfo')
        .then(data => {
            if (!data.success) return toast(data.text, {
                type: toast.TYPE.ERROR
            });
            const { referrals, percent, totalProfit } = data;
            localStorage.setItem('referrals', JSON.stringify(referrals));
            this.setState({ referrals, percent, totalProfit });
        });
    }

    showReferral(event) {
        event.preventDefault();
        this.setState(prevState => {
            prevState.myReferralsShown = !prevState.myReferralsShown;
            return prevState;
        });
    }
    
    checkPercent(expectedPercent) {
        const percent = this.state.percent * 100;
        if (percent == expectedPercent) return 'refer-half'
        if (percent < expectedPercent) {
            return '';
        } else {
            return 'refer_full'
        };
    }

    render() {
        const { referrals, percent, totalProfit } = this.state;

        return (
            <div className="card">
                <ToastContainer />
                <div className="card-body">
                    <p className="text-center">
                    Количество рефералов для получения определённого процента может быть вычислено по формуле: 1000 * 2^((желаемый процент - 0.5) * 10).
                    Для примера что бы расчитать сколько необходимо рефералов для получения 0.9% вы можете использовать следующую формулу: 1000 * 2^((0.9 - 0.5) * 10) = 1000 * 2^4 = 16000
                    </p>
                    <div className="d-flex justify-content-center  refer-block">
                    <div className={`d-flex flex-wrap text-center refer ${this.checkPercent(0.5)} justify-content-center half-rezult`}>
                        <span className="perсent">0.5%</span>
                        <span>0 рефералов</span>
                    </div>
                    <div className={`d-flex flex-wrap text-center refer ${this.checkPercent(0.6)}  justify-content-center`}>
                        <span className="perсent">0.6%</span>
                        <span>2 000 рефералов</span>
                    </div>
                    <div className={`d-flex flex-wrap text-center refer ${this.checkPercent(0.7)} justify-content-center full-rezult`}>
                        <span className="perсent">0.7%</span>
                        <span>4 000 рефералов</span>
                    </div>
                    <div className={`d-flex flex-wrap text-center refer ${this.checkPercent(1)} justify-content-center`}>
                        <span className="perсent">1%</span>
                        <span>32 000 рефералов</span>
                    </div>
                    <div className={`d-flex flex-wrap text-center refer ${this.checkPercent(2)} justify-content-center`}>
                        <span className="perсent">2%</span>
                        <span>32 768 000 рефералов</span>
                    </div>
                    </div>
                    <div className="d-flex justify-content-center flex-column flex-md-row justify-content-md-between mb-3">
                    <div className=" text-center  text-md-left mb-md-1 mb-3">
                        <a href="#" onClick={this.showReferral.bind(this)} className="btn btn-default btn-sm" id="referAnchor">{this.state.myReferralsShown ? 'Скрыть моих рефералов' : 'Показать моих рефералов'}</a>
                    </div>
                    <div className="ref-rezult text-center text-small text-md-right mb-md-1 mb-3">
                        <div>Ваш процент: {(percent * 100).toFixed(2)}%</div>
                        <div>Всего заработано: {totalProfit.toFixed(2)} <span className="flaticon-coins" /></div>
                    </div>
                    </div>
                    <div className={`${!this.state.myReferralsShown ? 'collapse' : ''} refer-collapse`} id="referCollapse">
                    <div className="table-responsive">
                        <table className="table table-striped table-top  table-support">
                        <thead>
                            <tr>
                            <th scope="col">Пользователь</th>
                            <th scope="col">Ваш доход</th>
                            <th scope="col">Пользователь зарегистрировался</th>
                            <th scope="col">Последняя ставка</th>
                            </tr>
                        </thead>
                        <tbody>
                            {referrals.map(referral => 
                                <tr>
                                    <td><a href="#" className="link-line"><img src={referral.user.avatar} className="avatar" alt />{referral.user.username}</a></td>
                                    <td><span className="flaticon-coins" /> {referral.profit.toFixed(2)}</td>
                                    <td>{referral.user.created_at}</td>
                                    <td>{referral.user.last_bet_time}</td>
                                </tr>
                            )}
                        </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
        )
    }
}