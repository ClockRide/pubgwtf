import { loadData } from "../helpers/loadData";

export function activateCode(ref_code, toast, event, cb) {
    if (event) event.preventDefault();
    loadData('/activateRefCode', {
        ref_code
    })
    .then(data => {
        if (data.success && typeof toast !== "undefined") toast(data.text, {
            type: toast.TYPE.SUCCESS
        });
        if (typeof cb !== "undefined") cb(data);
    })
}