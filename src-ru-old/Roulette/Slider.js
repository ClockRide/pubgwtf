import React, { Component } from 'react';
import { getItemImage } from '../helpers/getItemImage';
import { getRandomInteger } from '../helpers/getRandomInteger';

const finishPositionFrom = -7640;
const finishPositionTo = -7480;

export class Slider extends Component {
    constructor(props) {
        super(props);
        let avatars = [];
        try {
            avatars = JSON.parse(this.props.game.avatars);
        } catch (err) {
            console.log('Error: ', err);
        }

        this.state = {
            sliderStarted: false,
            finishPosition: getRandomInteger(finishPositionFrom, finishPositionTo),
            avatars
        };
        setTimeout(() => this.setState({sliderStarted: true}), 100);
    }

    shuffle(a) {
        for (let i = a.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [a[i], a[j]] = [a[j], a[i]];
        }
        return a;
    }

    render() {
        const { game, historyGame, resultsVisible, winningItem } = this.props;
        const { avatars } = this.state;
        let items = [];
        for (let bet of game.bets) 
            for (let item of bet.items)
                items.push(item);
        let winnerItem = items.find(item => item.ticket_from <= game.winner_ticket && item.ticket_to >= game.winner_ticket);
        if (typeof winnerItem == "undefined") winnerItem = {};
        const transparentBase64 = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=';
        return (
            <div className="card-line">
                <div className="slide-line card-slide" style={{width: '850px'}}>
                    <ul className="nav flex-nowrap justify-content-center" 
                    style={{transition: `all ${historyGame ? '0s' : '8s'} cubic-bezier(0.215, 0.61, 0.355, 1)`, marginLeft: this.state.sliderStarted ? `${this.state.finishPosition}px` : '0px'}}>
                        {avatars.map((user, index) => 
                            <li key={index}><a href="#"><img src={user.avatar} style={{
                                width: '85px',
                                height: '85px'
                            }}/></a></li>
                        )}
                    </ul>
                </div>
                <div className="panel-center"
                style={{backgroundImage: resultsVisible ? 'url(/assets/img/win.png)' : `url(${transparentBase64})`}}> 
                    <span className="panel-item">
                        <img style={{width: '60px'}} src={resultsVisible ? winnerItem.image : transparentBase64} alt />
                    </span>
                    <div className="game-number">{resultsVisible ? '#' + winnerItem.ticket_from + ' - ' + winnerItem.ticket_to : ''}</div>
                </div>
            </div>
        )
    }
}