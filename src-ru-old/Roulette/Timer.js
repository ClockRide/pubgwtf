import React, { Component } from 'react';
import { GameConfig } from '../config/GameConfig';
import openSocket from 'socket.io-client';

const socket = openSocket(':7020', {
    transports: ['websocket']
});

export class Timer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            timer: GameConfig.timer
        }
    }

    formatNumber(number) {
        number = number.toString();
        if (number.length === 0) return '00';
        if (number.length === 1) return '0' + number;
        return number; 
    }

    getMinutes() {
        return this.formatNumber(Math.floor(this.state.timer / 60));
    }
    
    getSeconds() {
        return this.formatNumber(this.state.timer - this.getMinutes() * 60);
    }

    componentDidMount() {
        socket.on('timer', this.handleTimer.bind(this));
        socket.on('newGame', this.handleTimer.bind(this, GameConfig.timer));
    }

    handleTimer(timer) {
        this.setState({
            timer
        })
    }

    render() {
        return (
            <div className="col-lg-3">
                <div className="timer d-flex align-items-center justify-content-center  justify-content-lg-between">
                    <div className="border">{this.getMinutes()}</div>
                    <span>:</span>
                    <div className="border">{this.getSeconds()}</div>
                </div>
            </div>
        )
    }
}