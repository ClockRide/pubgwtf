import React, { Component } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import { Stats } from './Stats';
import { Info } from './Info';
import { loadData } from '../helpers/loadData';
import { getItemImage } from '../helpers/getItemImage';
import { GameStates } from '../config/GameStates';
import { GameConfig } from '../config/GameConfig';
import { TradeLink } from './TradeLink';
import { EndGame } from './EndGame';
import { GameHeader } from './GameHeader';
import { Link } from 'react-router-dom';
import { groupBy } from 'underscore';

import openSocket from 'socket.io-client';

export class Roulette extends Component {
    constructor(props) {
        super(props);

        const defaultGame = {
            id: 'LOADING...',
            items: [],
            bets: [],
            usersInGame: []
        };
        
        const game = JSON.parse(localStorage.getItem(this.getGameKeyName()));
        const user = JSON.parse(localStorage.getItem('user'));
        let soundOn = localStorage.getItem('soundOn');
        if (soundOn === null) {
            localStorage.setItem('soundOn', 1);
            soundOn = 1;
        }

        this.state = {
            game: game === null ? defaultGame : game,
            soundOn: soundOn == 1,
            hideLinkInput: false,
            lastNotify: '',
            startIndex: 0,
            user
        }
        this.loadGame();
        this.loadUser();
    }
    
    getGameKeyName() {
        const { gameId } = this.props.match.params;
        const gameKeyName = `game_${gameId}`;
        return gameKeyName;
    }

    componentWillUnmount() {
        ['newBet', 'endGame', 'newGame', 'offerProceedStatus'].forEach(item => this.socket.off(item));
    }

    componentDidMount() {
        this.betSound = new Audio('/assets/sounds/new-item.mp3');
        this.betSound.volume = 0.5;
        
        this.socket = openSocket(':7020', {
            transports: ['websocket']
        });
        if (!this.props.historyGame) {
            this.socket.on('newBet', this.newBet.bind(this));
            this.socket.on('endGame', this.endGame.bind(this));
            this.socket.on('newGame', this.newGame.bind(this));
            this.socket.on('offerProceedStatus', this.offerProceedStatus.bind(this));
        }
    }

    offerProceedStatus(data) {
        try {
            data = JSON.parse(data);
        } catch (err) {
            return console.error('Ошибка обработки ставки');
        }
        if (this.state.user !== null && data.steamid64 == this.state.user.steamid64 && this.state.lastNotify != data.text) {
            toast(data.text, {
                type: data.success ? toast.TYPE.SUCCESS : toast.TYPE.ERROR
            });
            this.setState({
                lastNotify: data.text
            });
        }
    }
    
    toggleSoundMute(event) {
        event.preventDefault();
        this.setState(prevState => {
            prevState.soundOn = !prevState.soundOn;
            localStorage.setItem('soundOn', prevState.soundOn ? '1' : '0');
            return prevState;
        });
    }

    hideLinkInput() {
        this.setState({hideLinkInput: true});
    }

    newBet(newBet) {
        try {
            newBet = JSON.parse(newBet);
        } catch (err) {
            return console.error('Ошибка обработки ставки');
        }
        if (this.state.soundOn) this.betSound.play();
        if (this.state.game === null) return this.loadGame();
        this.setState(prevState => {
            prevState.game.bets.unshift(newBet);
            prevState.game.price = this.safetyAdd(prevState.game.price, newBet.price);
            prevState.game.bets = prevState.game.bets.map(bet => {
                if (typeof bet.items === "string") bet.items = JSON.parse(bet.items);
                if (bet.user_id == newBet.user_id) {
                    bet.chance = newBet.chance;
                } else {
                    const betsSum = prevState.game.bets.filter(bet1 => bet1.user_id == bet.user_id).map(bet => bet.price).reduce((prev, curr) => prev + parseFloat(curr), 0);
                    bet.chance = (betsSum / (prevState.game.price / 100)).toFixed(1);
                }
                bet.items = bet.items.map((item, i) => {
                    item.ticket_from = Math.round(bet.ticket_from + bet.items.slice(0, i).map(item => item.price * 100 * 100).reduce((prev, curr) => prev + curr, 0));
                    item.ticket_to = Math.round(item.ticket_from - 1 + item.price * 100 * 100);
                    return item;
                }).sort((a, b) => b.ticket_to - a.ticket_to);
                return bet;
            })
            .sort((a, b) => b.id - a.id);
                const groupedBets = groupBy(prevState.game.bets, bet => bet.user_id);
                prevState.game.usersInGame = Object.values(groupedBets).map(bet => {
                return {
                    avatar: bet[0].user.avatar,
                    steamid64: bet[0].user.steamid64,
                    chance: parseFloat(bet[0].chance)
                }
            });
            // if (prevState.game.usersInGame.findIndex(user => user.id == newBet.user_id) === -1) {
            //     newBet.user.chance = newBet.chance;
            //     prevState.game.usersInGame.push(newBet.user);
            // }
            const items = newBet.items;
            for (let item of items) prevState.game.items.push(item);
            try {
                localStorage.setItem(this.getGameKeyName(), JSON.stringify(prevState.game));
            } catch (err) {
                localStorage.clear();
                localStorage.setItem(this.getGameKeyName(), JSON.stringify(prevState.game));
            }
            return prevState;
        });
    }

    endGame(newGame) {
        try {
            newGame = JSON.parse(newGame);
        } catch (err) {
            return console.error('Ошибка окончания игры');
        }
        try {
            localStorage.setItem(this.getGameKeyName(), JSON.stringify(newGame));
        } catch (err) {
            localStorage.clear();
            localStorage.setItem(this.getGameKeyName(), JSON.stringify(newGame));
        }
        newGame.bets = newGame.bets.sort((a, b) => b.id - a.id)
        .map(bet => {
            if (typeof bet.items === "string") bet.items = JSON.parse(bet.items);
            bet.items = bet.items.map((item, i) => {
                item.ticket_from = Math.round(bet.ticket_from + bet.items.slice(0, i).map(item => item.price * 100 * 100).reduce((prev, curr) => prev + curr, 0));
                item.ticket_to = Math.round(item.ticket_from - 1 + item.price * 100 * 100);
                return item;
            }).sort((a, b) => b.ticket_to - a.ticket_to);
            return bet;
        });
        this.setState({
            game: newGame
        });
    }

    newGame(game) {
        try {
            game = JSON.parse(game);
        } catch (err) {
            return console.error('Ошибка начала новой игры');
        }
        try {
            localStorage.setItem(this.getGameKeyName(), JSON.stringify(game));
        } catch (err) {
            localStorage.clear();
            localStorage.setItem(this.getGameKeyName(), JSON.stringify(game));
        }
        this.setState({
            game,
            startIndex: 0
        });
    }

    safetyAdd(a, b) {
        return (parseFloat(a) + parseFloat(b)).toFixed(2);
    }

    loadGame() {
        const { gameId } = this.props.match.params;
        loadData(typeof gameId !== "undefined" ? `/getGame/${gameId}` : '/getGame')
        .then(data => {
            let { game } = data;
            if (game === null) return toast('Ошибка загрузки игры', {
                type: toast.TYPE.ERROR
            });
            game.bets = game.bets.sort((a, b) => b.id - a.id)
            .map(bet => {
                if (typeof bet.items === "string") bet.items = JSON.parse(bet.items);
                bet.items = bet.items.map((item, i) => {
                    item.ticket_from = Math.round(bet.ticket_from + bet.items.slice(0, i).map(item => item.price * 100 * 100).reduce((prev, curr) => prev + curr, 0));
                item.ticket_to = Math.round(item.ticket_from - 1 + item.price * 100 * 100);
                    return item;
                }).sort((a, b) => b.ticket_to - a.ticket_to);
                return bet;
            });
            this.setState({ game });
            try {
                localStorage.setItem(this.getGameKeyName(), JSON.stringify(game));
            } catch (err) {
                localStorage.clear();
                localStorage.setItem(this.getGameKeyName(), JSON.stringify(game));
            }
        })
        .catch(err => {
            console.error(err);
        });
    }

    loadUser() {
        loadData('/getUser')
        .then(data => {
            const { user } = data;
            this.setState({ user });
            localStorage.setItem('user', JSON.stringify(user));
        })
        .catch(err => {
            console.error(err);
        });
    }

    changeStartIndex(change, event) {
        event.preventDefault();
        this.setState(prevState => {
            const { game, startIndex } = prevState; 
            if (startIndex + change < 0) return prevState;
            if (game.usersInGame.length < 10) return prevState;
            if (game.usersInGame.length - (startIndex + change) < 10) return prevState;
            prevState.startIndex += change;
            return prevState;
        });
    }

    pickColor(index) {
        index = index % 5;
        if (index === 4) return 'red';
        if (index === 3) return 'pink';
        if (index === 2) return 'orange';
        if (index === 1) return 'caption-info';
        if (index === 0) return 'warning';
    }

    handleProfileOpen(steamid64, event) {
        event.preventDefault();
        this.props.openProfile(steamid64);
    }

    render() {
        let { game, user, soundOn, startIndex } = this.state;
        //document.title = `${parseFloat(game.price).toFixed(2)} - betsdota22.ru`;
        let usersInSlider = game.usersInGame.sort((a, b) => {
            if (a.chance > b.chance) return -1;
            if (a.chance < b.chance) return 1;
            if (a.steamid64 > b.steamid64) return -1;
            if (a.steamid64 < b.steamid64) return 1;
            return 0;
        });
        game.bets = game.bets.map(bet => {
            if (typeof bet.items === "string") bet.items = JSON.parse(bet.items);
            return bet;
        });
        //if (usersInSlider.length > 0 && usersInSlider.length < 7 && usersInSlider.length % 2 == 0) usersInSlider.push('');
        // const transparentSrc = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=';
        // if (usersInSlider.length > 0 && usersInSlider.length < 8) {
        //     const avatarsNeed = Math.floor((8 - usersInSlider.length) / 2);
        //     for (let i = 0; i < avatarsNeed; i++) usersInSlider.unshift('');
        //     console.log(avatarsNeed, usersInSlider);
        // }
        return (
            <div>
                {/* <div className="card">
                    <div className="card-body">
                        <div className="title text-center"><h1>Рулетка PUBG от 1 рубля</h1></div>
                        <p>PUBGWTF.COM - это рулетка CS GO для бомжей с минимальной ставкой 1 рубль. Чтобы принять участие в игре, вам необходимо пройти авторизацию через свой Steam аккаунт. После того, как вы прошли авторизацию в нашей системе, вы сможете внести абсолютно любой депозит.</p>
                    </div>
                </div> */}
                {user !== null && user.access_token === null && !this.state.hideLinkInput ? <TradeLink hideLinkInput={this.hideLinkInput.bind(this)} user={user} /> : ''}
                <Stats />
                {/* <Info /> */}
                <div className="title-row d-flex">
                    Game #{game.id}
                    <a href="#" onClick={this.toggleSoundMute.bind(this)} className="ml-auto "><span className={soundOn ? `flaticon-sound-on` : `flaticon-volume-off-indicator`} /></a>
                </div>
                {game.state === GameStates.FINISHED ? <EndGame game={game} historyGame={this.props.historyGame} /> : <GameHeader user={user} game={game} />}
                {game.usersInGame.length > 0 ? (
                    <div className="slide-line" style={{height: '89px'}}>
                        <ul className={`nav flex-nowrap ${usersInSlider.length < 9 ? 'justify-content-center' : ''}`}>
                            {usersInSlider.filter((user, index) => index >= startIndex).map((user, index) => (
                                <li>
                                    <noindex>
                                        <a href="#" onClick={this.handleProfileOpen.bind(this, user.steamid64)} >
                                            <img src={user.avatar} style={{width: '87px', height: '87px'}} alt /> 
                                            <span className={`caption ${this.pickColor(index)}`}>{user.chance}%</span> 
                                        </a>
                                    </noindex>
                                </li>
                            ))}
                        </ul>
                        {usersInSlider.length > 9 ? <a href="#" onClick={this.changeStartIndex.bind(this, -1)} className="slide-prew"><span>пред</span></a> : ''}
                        {usersInSlider.length > 9 ? <a href="#" onClick={this.changeStartIndex.bind(this, +1)} className="slide-next"><span>след</span></a> : ''}
                    </div>
                ) : ''}
                <div className="flex-table">
                    {game.bets.map(bet =>
                        bet.items.map((item, index) => 
                            <div style={{backgroundColor: item.color}} className={`d-flex justify-content-between flex-md-nowrap align-items-stretch table-item flex-wrap flex-sm-wrap ${this.pickColor(index)}`}>
                                <div className="item-first">
                                    <noindex>
                                        <a href="#" onClick={this.handleProfileOpen.bind(this, bet.user.steamid64)} class="avatar">
                                            <img src={bet.user.avatar} className="img-fluid avatar" alt />
                                        </a>
                                    </noindex>
                                </div>
                                <div className="item-text flex-column">
                                    <div>
                                    {bet.user.username} внёс {item.name} <span className="nowrap"><span className="flaticon-coins" /> {parseFloat(item.price).toFixed(2)}</span>
                                    </div>
                                    <div className="strong">Билеты: от #{item.ticket_from} до #{item.ticket_to}</div>
                                </div>
                                <div className="item-last">
                                    <img src={typeof item.image === "undefined" ? getItemImage(item.classid) : item.image} alt />
                                    <span><i>+{Math.ceil(item.price * 100 * 100)}</i></span>
                                </div>
                            </div>
                        )
                    )}
                </div>
                <div className="game-play border d-flex flex-wrap flex-md-nowrap justify-content-center justify-content-md-start align-items-center bg-text p-3 pl-md-4 pr-md-4">
                    <div className="game-text">
                    <div className="play-title">ИГРА НАЧАЛАСЬ! ДЕЛАЙТЕ СТАВКИ!</div>
                    <div className="d-flex flex-wrap flex-md-nowrap align-items-center">
                        <Link to="/fair" className="btn btn-dark btn-sm mx-auto mr-md-4">Честная игра</Link>
                        <div><span class="wrap wrap_hash">Хэш: {game.hash}</span></div>
                    </div>
                    </div>
                </div>
                {/* <div className="card keywords-block">
                    <div className="card-body p-3">
                    <div className="title text-center mb-3"><h1 class="h4">Рулетка PUBG для бомжей от 1 рубля</h1></div>
                            <p class="mb-2">Рулетка PUBG для бомжей от 1 рубля – PUBWTF.COM. Мы предлагаем владельцам игры PLAYERUNKNOWNS BATTLEGROUNDS умножить свои скины на режиме roulette pubg. Каждые 2 минуты запускается новое сражение. Присоединиться к любому поединку довольно просто. Игроку нужно при себе иметь рабочий Steam клиент, игровые вещи в инвентаре. Если ваши игровые вещи, официальная торговая площадка Steam заблокировала на 7 дней, то приходите к нам после того, как они станут доступны для обмена. Допустим вам очень хочется поиграть прямо сейчас. Пока не пропало желание. Посетите магазин скинов PUBG. На нём есть огромное количество шмоток, который не имеют ограничений на передачу с бота на бот.</p>
                            <p class="mb-2">Рулетка PUBG от 1 рубля иногда помогает стать богачом абсолютно нищему бичу. Иногда достаточно сделать минимальную ставку, и надеется на чудо, которое редко улыбается. Шансы, разумеется, будут самые низкие, но как показывает история игр, этого порою достаточно для победы. Если вас одолевают сомнения, просто наблюдайте за процессом. Тот, кто не рискует, ничего не тратит. Совершая обмены с нашим игровым ботом, вы соглашаетесь с нашими правилами. Поэтому настоятельно рекомендуем ознакомиться с ними подробно. Специалисты технической поддержки составили очень простые правила, и будут рады вам ответить на существующие у вас вопросы.</p>
                            <p class="mb-2">Выиграть jackpot в любом из раундов режима roulette pubg довольно просто. Для этого необходимо сделать максимальную ставку от суммы раунда. Чем выше ваш процент на победу, тем больше шансов сорвать крупный банк. Но, даже у тех игроков, кто поставил меньше всех, есть шанс стать чемпионом. Наши игроки выбирают честные рулетки PUBG для бомжей с минимальной ставкой от 1 рубля за простоту и открытость. Придумайте свою стратегию. Также используйте тактику других успешных финалистов. Проводите анализ турниров. И после этого рулетка PUBG для бомжей падёт под вашим натиском.</p>
                    </div>
                </div> */}
            </div>
        )
    }
}