import React, { Component } from 'react';
import sha256 from 'sha256';

export class Fair extends Component {
    constructor(props) {
        super(props);

        this.state = {
            ticketsCount: '',
            hash: '',
            secret: '',
            secret_word: '',
            winnerTicket: ''
        }
    }

    checkFair(event) {
        event.preventDefault();
        const {
            ticketsCount,
            secret,
            secret_word,
            hash
        } = this.state;
        const expectedHash = sha256(secret.toString() + secret_word.toString());
        const hashCorrect = expectedHash == hash;
        const winnerTicket = Math.ceil(ticketsCount * secret);
        this.setState({
            winnerTicket: `Winning ticket - ${winnerTicket}, hash ${hashCorrect ? 'corresponds' : "doesn't corresponds"}`
        });
    }

    render() {
        return (
            <div className="card-block">
                <div class="title-row d-flex justify-content-center mb-3">Честная игра</div>
                <div className="card">
                    <h5 className="card-title">Как это работает?</h5>
                    <div className="card-body">
                    <p className="card-text">Наша система проверки честности выбирает победителя с помощью <span className="highlight">Числа раунда</span>, которое генерируется в начале игры. <span className="highlight">Число раунда</span> хэшируется используя SHA256 хэш который доступен для просмотра всем желающим.</p>
                    <p className="card-text">В конце розыгрыша сайт показывает <span className="highlight">Число раунда</span>, которое было хэшировано в начале игры, вы можете убедиться в том что <span className="highlight">Число раунда</span> не изменялось в течении игры <span className="highlight">Число раунда</span> умножается на общее количество предметов в результате чего определяется выигрышный билет.</p>
                    <p className="card-text">И владелец выигрышного предмета становится победителем. Просто и эффективно, не так ли?</p>
                    </div>
                </div>
                <div className="card">
                    <h5 className="card-title">Термины</h5>
                    <div className="card-body">
                    <p className="card-text">
                        <span className="text-title">Число раунда</span> Случайное дробное число в промежежутке от 0 to 1 (пример: 0.349269263810378)
                    </p>
                    <p className="card-text">
                        <span className="text-title">Хэш</span> SHA256 хэшируется из числа раунда, используется что бы доказать честность игры.
                    </p>   
                    <p className="card-text">
                        <span className="text-title">Билет</span> За каждую 1/10000 часть коина вы получаете 1 билет (1 коин = 10 000 билетов).</p>
                    </div>
                </div>
                <div className="card">
                    <h5 className="card-title">Выбор победителя</h5>
                    <div className="card-body card-text__margin">
                    <p>Каждая ставка оценивается определённым количеством билетов. Номера билетов назначаются в завимости от времена ставки</p>
                    <p>Выигрышный билет определяется по формуле: ceil(количество билетов в игре * число раунда) = выигрышный билет (ceil() функция округляющая число в большую сторону).</p>
                    <p>Пользователь которому пренадлежит выигрышный билет назначается победителем игры</p>
                    </div>
                </div>
                <div className="card">
                    <h5 className="card-title">Проверка</h5>
                    <div className="card-body card-text__margin">
                    <p>
                        Вы можете использовать представленную ниже форму для проверки соответствия хэша числа раунда и расчёта выигрышного билета. 
                        Так же вы можете проверить SHA256 хэш на представленных ниже сайтах
                        <ul>
                            <li>
                                <noindex><a href="http://www.xorbin.com/tools/sha256-hash-calculator" target="_blank" rel="nofollow">xorbin.com</a></noindex>
                            </li>
                            <li>
                                <noindex><a href="https://passwordsgenerator.net/sha256-hash-generator/" target="_blank" rel="nofollow">passwordsgenerator.net</a></noindex>
                            </li>
                            <li>
                                <noindex><a href="https://hash.online-convert.com/ru/sha256-generator" target="_blank" rel="nofollow">hash.online-convert.com</a></noindex>
                            </li>
                            <li>
                                <noindex><a href="http://www.md5calc.com/sha256" target="_blank" rel="nofollow">md5calc.com</a></noindex>
                            </li>
                            <li>
                                <noindex><a href="http://www.convertstring.com/ru/Hash/SHA256" target="_blank" rel="nofollow">convertstring.com</a></noindex>
                            </li>
                        </ul>
                    </p>
                    <form onSubmit={this.checkFair.bind(this)}>
                        <div className="form-group">
                        <input value={this.state.ticketsCount} onChange={event => this.setState({ticketsCount: event.target.value})} type="text" className="form-control" placeholder="Количество билетов" />
                        </div>
                        <div className="form-group">
                        <input value={this.state.secret} onChange={event => this.setState({secret: event.target.value})} type="text" className="form-control" placeholder="Число раунда" />
                        </div>
                        <div className="form-group">
                        <input value={this.state.hash} onChange={event => this.setState({hash: event.target.value})} type="text" className="form-control" placeholder="Хэш" />
                        </div>
                        <div className="row  no-gutters">
                        <div className="col-sm-4 col-md-3 col-lg-2" style={{paddingRight: '16px'}}>
                            <div className="form-group">
                                <button type="submit" className="btn btn-block btn-sm btn-default btn-left">Проверить</button>
                            </div>
                        </div>
                        <div className="col-sm-8  col-md-9 col-lg-10 ">
                            <div className="form-group">
                                <input type="text" className="form-control test-result" readOnly value={this.state.winnerTicket} />
                            </div>
                        </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        )
    }
}