import React, { Component, Fragment } from 'react';
import { loadData } from '../helpers/loadData';
import { ToastContainer, toast } from 'react-toastify';

const TICKETS_STATES = {
    0: 'ожидает',
    1: 'закрыт'
}

export class Ticket extends Component {
    constructor(props) {
        super(props);

        this.state = {
            ticket: {
                messages: [
                    {
                        user: {}
                    }
                ]
            },
            ticketText: ''
        }
        this.loadTicket();
    }

    replyTo(username) {
        this.setState({
            ticketText: `${username}, `
        });
    }

    loadTicket() {
        const { ticketId } = this.props.match.params;
        loadData('/getTicket', { ticketId })
        .then(data => {
            const { ticket } = data;
            this.setState({ ticket })
        })
        .catch(err => {
            toast('Ошибка загрузки тикетов', {
                type: toast.TYPE.ERROR
            });
        })
    }

    addMessage(event) {
        event.preventDefault();
        const { ticketText } = this.state;
        const { ticketId } = this.props.match.params;
        loadData('/addMessageToTicket', { ticketText, ticketId })
        .then(data => {
            toast(data.text, {
                type: data.success ? toast.TYPE.SUCCESS : toast.TYPE.ERROR
            });
            if (data.success) {
                this.setState({ ticketText: '' });
                this.loadTicket();
            }
        })
        .catch(err => {
            toast('Ошибка отправки сообщения', {
                type: toast.TYPE.ERROR
            });
        })
    }

    render() {
        const { ticket, ticketText } = this.state;
        return (
            <div className="card-block">
                <ToastContainer />
                <div class="title-row d-flex justify-content-center mb-3">Поддержка</div>
                <div class="center-title p-3 text-center">Тикет #{ticket.id}</div>
                <div className="card ">
                    <div className="card-body">
                        <p className="mb-2 mt-2">Этот тикет {TICKETS_STATES[ticket.state]}</p>
                        <p className="mb-4">Примерное время ожидания - 5 часов</p>
                        <div className="table-responsive  mb-2">
                        <table className="table table-striped table-top   table-support">
                            <tbody>
                                {ticket.messages
                                .map(message => {
                                    if (message.user === null) message.user = {
                                        username: 'Support',
                                        avatar: '/assets/img/support-avatar.png'
                                    }
                                    return message;
                                })
                                .map((message, index) => (
                                    <tr>
                                        <td>#{index + 1}</td>
                                        <td>
                                            <a onClick={this.replyTo.bind(this, message.user.username)} href="#" className="link-line">
                                                <img src={message.user.avatar} className="avatar" alt />{message.user.username}
                                            </a>
                                        </td>
                                        <td>{message.text}</td>
                                        <td><span className="status ">{message.created_at}</span></td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                        </div>
                        <p className="mb-4">Максимальное время ответа - 12 часов. Вы можете ответить на тикет использую форму ниже</p>
                        <form onSubmit={this.addMessage.bind(this)} className="refer-form">
                        <div className="row mb-4">
                            <div className="col-12 col-lg-8 offset-lg-2">
                            <div className="form-group">
                                <textarea value={ticketText} onChange={event => this.setState({ticketText: event.target.value})} className="form-control" placeholder="Input text..." id="text" rows={5} />
                            </div>
                            <div className="row">
                                <div className="col-sm-6 mb-3 mb-sm-0">
                                    <button type="submit" className="btn btn-default btn-block">Отправить</button>
                                </div>
                                <div className="col-sm-6 mb-3 mb-sm-0">
                                    <button type="reset" className="btn btn-default btn-block ">Закрыть обращение</button>
                                </div>
                            </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}