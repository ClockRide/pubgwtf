import React, { Component } from 'react';
import { loadData } from '../helpers/loadData';

export class Top extends Component {
    constructor(props) {
        super(props);
        const users = JSON.parse(localStorage.getItem('usersTop'));
        const period = localStorage.getItem('topPeriod');

        this.state = {
            users: users === null ? [] : users,
            period: period === null ? 'day' : period
        };
        this.loadTop(this.state.period);
    }

    loadTop(period) {
        loadData(`/getTop/${period}`)
          .then(data => {
              const { users } = data;
              this.setState({
                  users
              });
              localStorage.setItem('usersTop', JSON.stringify(users));
          })
    }

    changePeriod(period) {
        this.setState({
            period
        });
        localStorage.setItem('topPeriod', period);
        this.loadTop(period);
    }

    isPeriodActive(period) {
        return this.state.period == period ? 'active' : '';
    }

    render() {
        const { users } = this.state;
        return (
            <div>
                <div class="title-row d-flex justify-content-center mb-3">Топ игроков</div>
                <nav className="nav nav-pills nav-fill filter">
                    <a className={`nav-item btn ${this.isPeriodActive('day')}`} onClick={this.changePeriod.bind(this, 'day')} href="#">Сегодня</a>
                    <a className={`nav-item btn ${this.isPeriodActive('week')}`} onClick={this.changePeriod.bind(this, 'week')} href="#">Неделя</a>
                    <a className={`nav-item btn ${this.isPeriodActive('month')}`} onClick={this.changePeriod.bind(this, 'month')} href="#">Месяц</a>
                    <a className={`nav-item btn ${this.isPeriodActive('year')}`} onClick={this.changePeriod.bind(this, 'year')} href="#">Год</a>
                </nav>
                <div className="table-responsive ">      
                    <table className="table table-striped table-top">
                        <thead>
                        <tr>
                            <th scope="col">Место</th>
                            <th scope="col">Профиль</th>
                            <th scope="col">Участий</th>
                            <th scope="col">Выигрышей</th>
                            <th scope="col">Процент побед</th>
                            <th scope="col">Сумма банков</th>
                        </tr>
                        </thead>
                        <tbody>
                            {users.map((user, index) => (
                                <tr>
                                    <td><span className="badge number number__empty">{index + 1}</span></td>
                                    <td>
                                        <div className="link-line">
                                            <noindex>
                                                <a href={`http://steamcommunity.com/profiles/${user.steamid64}`} target="_blank" rel="nofollow" class="avatar">
                                                <img src={user.avatar} className="avatar" alt />
                                                </a>{user.username}
                                            </noindex>
                                        </div>
                                    </td>
                                    <td>{user.joins}</td>
                                    <td>{user.wins}</td>
                                    <td>{user.winRate.toFixed(2)}%</td>
                                    <td><span className="flaticon-coins" />{parseFloat(user.bank).toFixed(2)}</td>
                                </tr>
                            ))}                                                      
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}