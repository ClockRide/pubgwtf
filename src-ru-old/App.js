import React, { Component } from 'react';
import { Footer } from './Footer';
import { Header } from './Header';
import { Main } from './Main';
import { Chat } from './Chat';
import { ProfileModal } from './ProfileModal';
import { activateCode } from './Referral/activateCode';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
        profileOpened: false,
        profileSteamid64: 0
    };
    const refCode = this.findGetParameter('r');
    if (refCode !== null) {
      activateCode(refCode, undefined, undefined, data => document.location = '/');
    }
  }

  findGetParameter(parameterName) {
    let result = null,
        tmp = [];
    window.location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
          tmp = item.split("=");
          if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}

  openProfile(steamid64) {
      this.setState({
          profileOpened: true,
          profileSteamid64: steamid64
      });
  }

  closeProfile() {
      this.setState({
          profileOpened: false
      });
  }

  render() {
    return (
      <div style={{minWidth: '100%'}}>
        <div className="cover-container d-flex mx-auto flex-column">
          <Header />
          <Main openProfile={this.openProfile.bind(this)} />
          <Footer />
          {this.state.profileOpened ? <div class="modal-backdrop fade show" onClick={this.closeProfile.bind(this)} /> : ''}
        </div>
        <ProfileModal profileOpened={this.state.profileOpened} steamid64={this.state.profileSteamid64} closeProfile={this.closeProfile.bind(this)} />
        <Chat />
      </div>
    );
  }
}

export default App;