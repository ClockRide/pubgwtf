import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { User } from './User';
import { loadData } from './helpers/loadData';

export class Header extends Component {
    constructor(props) {
        super(props);

        this.state = {
            navbarCollapsed: true,
            user: JSON.parse(localStorage.getItem('user'))
        }
    }

    getUser() {
        loadData('/getUser')
        .then(data => {
            const { user } = data;
            this.setState({
                user
            });
            localStorage.setItem('user', JSON.stringify(user));
        })
    }

    toggleCollapse(event) {
        this.setState(prevState => {
            prevState.navbarCollapsed = !prevState.navbarCollapsed;
            return prevState;
        });
    }

    render() {
        const { user } = this.state;

        return (
            <header>
                <nav className="navbar navbar-expand-lg  navbar-dark bg-dark   hidden-lg">
                    <div className="d-flex flex-nowrap  justify-content-between w-100">
                    <Link className="navbar-brand" to="/"><img src="/assets/images/logo.png" alt="PUBGWTF.COM" title="PUBGWTF.COM" className="img-fluid" /></Link>
                    <button className="navbar-toggler" type="button" onClick={this.toggleCollapse.bind(this)} aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon" />
                    </button>
                    </div>
                    <div className={`${this.state.navbarCollapsed ? 'collapse' : ''} navbar-collapse`}>
                    <div className="  nav-masthead mx-auto ">
                        <div className="row">
                        {user === null ? <div className="col-12"><a href="/login" className="btn btn-lg btn-sign btn-block"><span>Войти</span></a></div> : ''}
                        <div className="col-sm-6"><Link className="nav-link" to="/"><span>Играть</span></Link></div>
                        <div className="col-sm-6"><Link className="nav-link link__1" to="/about"><span>О нас</span></Link></div>
                        <div className="col-sm-6"><Link className="nav-link link__2" to="/top"><span>Топ игроков</span></Link></div>
                        <div className="col-sm-6"><Link className="nav-link link__3" to="/history"><span>История игр</span></Link></div>
                        <div className="col-sm-6"><Link className="nav-link link__4" to="/fair"><span>Честная игра</span></Link></div>
                        {user !== null ? <div className="col-sm-6"><Link className="nav-link link__5" to="/settings"><span>Настройки</span></Link></div> : ''}
                        <div className="col-sm-6"><Link className="nav-link link__6 " to="/shop"><span>Магазин</span></Link></div>
                        <div className="col-sm-6"><Link className="nav-link link__7" to="/referral"><span>Рефералка</span></Link></div>
                        <div className="col-sm-6"><Link className="nav-link link__8 " to="/support"><span>Поддержка</span></Link></div>
                        { /* <div className="col-sm-6"><a className="nav-link link__9" href="#"><span>Статьи</span></a> </div> */}
                        </div>
                    </div>
                    </div>
                </nav>
                <div className="container hidden-md-down">
                    <div className="d-flex flex-wrap flex-lg-nowrap align-items-center justify-content-center justify-content-lg-between preheader">
                    <Link to="/" className="mr-lg-5"><img src="/assets/images/logo.png" alt="PUBGWTF.COM" title="PUBGWTF.COM" className="img-fluid" alt /></Link>
                    <User />
                    </div>
                </div>
            </header>
        )
    }
}