import React, { Component } from 'react';
import { loadData } from './helpers/loadData';

export class ChatMessage extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            controlsOpened: false
        };
    }

    toggleControls(event) {
        this.setState(prevState => {
            prevState.controlsOpened = !prevState.controlsOpened;
            return prevState;
        });
    }

    deleteMessage(event) {
        event.preventDefault();
        const { messageJson } = this.props;
        loadData('/deleteMessage', {
            messageJson
        })
        .then(data => {
            const { success } = data;
            if (success) this.props.deleteMessage();
        })
        .catch(err => {
            console.error('Error with checking previleges');
        });
    }

    banChat(time, event) {
        event.preventDefault();
        const { messageJson } = this.props;
        const message = JSON.parse(messageJson);
        loadData('/banChat', {
            time,
            userId: message.user_id
        })
        .then(data => {
            const { success } = data;
            if (success) this.props.deleteMessage();
        })
        .catch(err => {
            console.error('Error with checking previleges');
        });
    }

    render() {
        const { messageJson, isAdmin } = this.props;
        const message = JSON.parse(messageJson);

        return (
            <li className="media">
                <img className="mr-2" src={message.avatar} style={{width: '46px', height: '46px'}} alt />
                <div className="media-body">
                {isAdmin ? (
                    <div className="close dropdown" onClick={this.toggleControls.bind(this)}>
                        <div 
                            className={`popover ${this.state.controlsOpened ? '' : 'fade'} bs-popover-bottom`} 
                            style={{display: this.state.controlsOpened ? 'block' : 'none'}}
                        >
                        <div className="arrow" />
                        <div className="text-name"></div>
                        <ul className="list-unstyled">
                            <li><a href="#" onClick={this.banChat.bind(this, 60 * 10)} className="list-link">Бан на 10 мин</a></li>
                            <li><a href="#" onClick={this.banChat.bind(this, 60 * 60 * 24)} className="list-link">Бан на сутки</a></li>
                            <li><a href="#" onClick={this.banChat.bind(this, 60 * 60 * 24 * 7)} className="list-link">Бан на неделю</a></li>
                            <li><a href="#" onClick={this.banChat.bind(this, 60 * 60 * 24 * 365 * 10)} className="list-link">Бан навечно</a></li>
                            <li><a href="#" onClick={this.deleteMessage.bind(this)} className="list-link">Удалить</a></li>
                        </ul>
                        </div>
                    </div>
                ) : ''}
                <a href="#" className=" name-link mt-0 mb-1">{message.username}</a>
                <p>{message.text}</p>
                <div className="time">{message.created_at}</div>
                </div>
            </li>
        )
    }
}