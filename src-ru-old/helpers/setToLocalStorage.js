export const setToLocalStorage = (items) => {
    let data = {};

    for (const item in items) {
        localStorage.setItem(item, items[item]);
    }
}