import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class About extends Component {
    render() {
        return (
            <div className="card-block">
                <div class="title-row d-flex justify-content-center mb-3">О нас</div>
                <div className="card">
                    <h5 className="card-title">О нас</h5>
                    <div className="card-body">
                        <p className="card-text">Добро пожаловать на BETSDOTA22.RU - сайт по умножение скинов Steam.</p>
                    </div>
                </div>
                <div className="card">
                    <h5 className="card-title">Как рулетка работает?</h5>
                    <div className="card-body">
                        <p className="card-text">Все участники ставят свои предметы из RUST, DOTA2, TF2, H1Z1 или Black Squad. Когда число поставленных вещей доходит до максимального, или таймер заканчивает отсчёт сайт выбирает победителя который забирает всё.</p>
                        <p className="card-text">Как это работает?</p>
                    </div>
                </div>
                <div className="card">
                    <h5 className="card-title">Всё очень просто</h5>
                    <div className="card-body">
                        <ol className="decimal-list">
                            <li>Вы ставите ваши предметы</li>
                            <li>Ваши предметы входят в игру и вы получаете 10 000 билетов за каждый коин стоимости поставленного предмета</li>
                        </ol>
                    </div>
                </div>
                <div className="card">
                    <h5 className="card-title">Главный принцип</h5>
                    <div className="card-body">
                        <p className="card-text">Больше ставка — больше шанс на победу! Но даже поставив 1 коин у вас остаётся шанс забрать весь банк!</p>
                    </div>
                </div>
                <div className="card">
                    <h5 className="card-title">Правила</h5>
                    <div className="card-body">
                        <ol className="decimal-list">
                            <li>Максимальная ставка представлена на главной странице.</li>
                            <li>Комиссия сайта — 10%. (Если ваш шанс больше или равен 90%, то комиссия = 0%)</li>
                            <li>Вы получите свои вещи сразу после окончания игры, но иногда отправка выигрыша может задержаться</li>
                            <li>Если вы не принимаете предложение обмена в течении часа он отменяется. Это необходимо для поддержания работоспособности наших ботов</li>
                            <li>Каждый раз когда вы ставите вещи вы подтверждаете своё согласие с данными правилами</li>
                            <li>Если ваш инвентарь закрыт или вы указали неверную ссылку на обмен пожалуйста обратитесь в поддержку, мы переотправим предложение обмена для вас</li>
                            <li>Таймер вы можете найти на главной странице.</li>
                            <li>Мы принимаем только вещи из игр DOTA 2, TF2, RUST, H1Z1 и Black Squad. Предложение обмена содержащее вещи из других игр будет отклонено.</li>
                            <li>Мы не несём отвественности за неверно оценённые вещи.</li>
                            <li>Мы не несем отвественности за вещи заблокированные на аккаунте бота в результате действий Steam!</li>
                        </ol>
                    </div>
                </div>
                <div className="card">
                    <h5 className="card-title">Приватность</h5>
                    <div className="card-body">
                        <ol className="decimal-list">
                            <li>Мы не сохраняем какие либо данные о вас кроме вашего никнейма, аватарки и трейд ссылки.</li>
                            <li>Ваша аватарка и имя пользователя доступны для всех в реальном времени</li>
                        </ol>
                    </div>
                </div>
            </div>
        )
    }
}